/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <functional>

#include <QHash>
#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "depend.hpp"
#include "report.hpp"

// ----------

namespace lvd::reports {

class Analyzer : public QObject {
  Q_OBJECT LVD_LOGGER

 private:
  using  Declaration  =                std::function<Analyzer*(QObject*)> ;
  using  Declarations = QHash<QString, std::function<Analyzer*(QObject*)>>;

  static Declarations& declarations();

 public:
  template <class T>
  static bool declare(const QString& species) {
    declarations()[species] = &T::create;
    return true;
  }

 protected:
  Analyzer        (QObject*       parent = nullptr);

 public:
  ~Analyzer() override;

  static
  Analyzer* create(const QString& species,
                   QObject*       parent = nullptr);

 public:
  void analyze     (const QString& line) {
    return analyze_impl(line);
  }

 protected:
  virtual
  void analyze_impl(const QString& line) = 0;

 signals:
  void report(const Report& report);
  void depend(const Depend& depend);
};

#ifndef DECLARE_ANALYZER
#define DECLARE_ANALYZER(Type, Name) \
  inline const bool Name ## __ ## Decl = lvd::reports::Analyzer::declare<Type>(#Name);
#endif  // DECLARE_ANALYZER

}  // namespace lvd::reports
