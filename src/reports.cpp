/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "reports.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <algorithm>

#include <QByteArray>
#include <QCollator>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QList>
#include <QLocale>
#include <QMetaObject>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QSettings>
#include <QStringRef>
#include <QVariant>

#include "lvd/shield.hpp"
#include "lvd/string.hpp"

#include "analyzer.hpp"
#include "config.hpp"
#include "reports_executor.hpp"
#include "vulnerability.hpp"

// ----------

namespace {
using namespace lvd;
using namespace lvd::reports;

void parse_singlex(QSettings&     qsettings,
                   const QString& key,
                   QStringList&   dst) {
  if (qsettings.contains(key)) {
    dst << qsettings.value(key).toString();
  }
}

void parse_several(QSettings&     qsettings,
                   const QString& key,
                   QStringList&   dst) {
  int size_i = qsettings.beginReadArray(key);
  LVD_FINALLY { qsettings.endArray(); };

  for (int i = 0; i < size_i; i ++) {
    qsettings.setArrayIndex(i);

    if (qsettings.contains(key)) {
      dst << qsettings.value(key).toString();
    }
  }
}

}  // namespace

// ----------

namespace lvd::reports {

Reports::Reports(const QString& config,
                 QObject*       parent)
    : QObject(parent),
      config_(config) {
  LVD_LOG_T();

  LVD_LOG_D() << "config:"
              << config;
}

Reports::~Reports() {
  LVD_LOG_T();
}

// ----------

void Reports::execute() {
  LVD_LOG_T();
  LVD_SHIELD;

  push_state(&Reports::state_finished);

  push_state(&Reports::state_publish_depends);
  push_state(&Reports::state_publish_reports);

  push_state(&Reports::state_combine_depends);
  push_state(&Reports::state_combine_reports);

  push_state(&Reports::state_execute_reports);

  push_state(&Reports::state_settings);

  exec_state();

  LVD_SHIELD_FUN(&Reports::failure);
}

// ----------

bool Reports::state_settings() {
  LVD_LOG_T();

  QSettings qsettings(config_, QSettings::IniFormat);
  qsettings.setFallbacksEnabled(false);

  if (reports_path_.isEmpty()) {
    if (qsettings.contains("reports_path")) {
      reports_path_ = qsettings.value("reports_path").toString();
    }
  }

  // TODO major (Backward compatibility)
  if (reports_path_.isEmpty()) {
    if (qsettings.contains("report")) {
      reports_path_ = qsettings.value("report"      ).toString();
    }
  }

  if (reports_path_.isEmpty()) {
    reports_path_ = "report.json";
  }
  // TODO major END

  LVD_LOG_D() << "reports_path:"
              << reports_path_;

  if (depends_path_.isEmpty()) {
    if (qsettings.contains("depends_path")) {
      depends_path_ = qsettings.value("depends_path").toString();
    }
  }

  LVD_LOG_D() << "depends_path:"
              << depends_path_;

  if (qsettings.contains("source")) {
    source_ = qsettings.value("source").toString();
  }
  { QFileInfo qfileinfo(source_);
    source_ = qfileinfo.absoluteFilePath(); }

  LVD_LOG_D() << "source:"
              << source_;

  if (qsettings.contains("binary")) {
    binary_ = qsettings.value("binary").toString();
  }
  { QFileInfo qfileinfo(binary_);
    binary_ = qfileinfo.absoluteFilePath(); }

  LVD_LOG_D() << "binary:"
              << binary_;

  bool ok;

  if (qsettings.contains("hard_timeout")) {
    QVariant hard_timeout = qsettings.value("hard_timeout");
    hard_timeout_ = hard_timeout.toInt(&ok);

    if (!ok) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "invalid hard timeout"
                          << hard_timeout;

      emit failure(message);
      return false;
    }
  } else {
    hard_timeout_ = config::Reports_Hard_Timeout();
  }

  LVD_LOG_D() << "hard_timeout:"
              << hard_timeout_;

  if (qsettings.contains("soft_timeout")) {
    QVariant soft_timeout = qsettings.value("soft_timeout");
    soft_timeout_ = soft_timeout.toInt(&ok);

    if (!ok) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "invalid soft timeout"
                          << soft_timeout;

      emit failure(message);
      return false;
    }
  } else {
    soft_timeout_ = config::Reports_Soft_Timeout();
  }

  LVD_LOG_D() << "soft_timeout:"
              << soft_timeout_;

  for (QString& name : qsettings.childGroups()) {
    Dossier     dossier;
    dossier.name = name;

    qsettings.beginGroup(name);

    LVD_FINALLY {
      qsettings.endGroup();
    };

    LVD_LOG_D() << "dossier:"
                << dossier.name;

    if (qsettings.contains("path")) {
      dossier.     path = qsettings.value("path").toString();
    }

    LVD_LOG_D() << "dossier path:"
                << dossier.path;

    if (qsettings.contains("keep")) {
      dossier.keep_path = qsettings.value("keep").toBool();
    }

    LVD_LOG_D() << "dossier keep:"
                << dossier.keep_path;

    if (qsettings.contains("hard_timeout")) {
      QVariant hard_timeout = qsettings.value("hard_timeout");
      dossier.hard_timeout = hard_timeout.toInt(&ok);

      if (!ok) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << "invalid hard timeout"
                            << hard_timeout
                            << "for dossier"
                            << dossier.name;

        emit failure(message);
        return false;
      }
    }

    LVD_LOG_D() << "dossier hard timeout:"
                << dossier.hard_timeout;

    if (qsettings.contains("soft_timeout")) {
      QVariant soft_timeout = qsettings.value("soft_timeout");
      dossier.soft_timeout = soft_timeout.toInt(&ok);

      if (!ok) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << "invalid soft timeout"
                            << soft_timeout
                            << "for dossier"
                            << dossier.name;

        emit failure(message);
        return false;
      }
    }

    LVD_LOG_D() << "dossier soft timeout:"
                << dossier.soft_timeout;

    QStringList subgroups = qsettings.childGroups();

    if        (qsettings.contains("before")) {
      parse_singlex(qsettings, "before", dossier.before);
    } else if (subgroups.contains("before")) {
      parse_several(qsettings, "before", dossier.before);
    }

    for (const QString& before : qAsConst(dossier.before)) {
      LVD_LOG_D() << "dossier before:"
                  << before;
    }

    if        (qsettings.contains("during")) {
      parse_singlex(qsettings, "during", dossier.during);
    } else if (subgroups.contains("during")) {
      parse_several(qsettings, "during", dossier.during);
    }

    for (const QString& during : qAsConst(dossier.during)) {
      LVD_LOG_D() << "dossier during:"
                  << during;
    }

    if        (qsettings.contains("beyond")) {
      parse_singlex(qsettings, "beyond", dossier.beyond);
    } else if (subgroups.contains("beyond")) {
      parse_several(qsettings, "beyond", dossier.beyond);
    }

    for (const QString& beyond : qAsConst(dossier.beyond)) {
      LVD_LOG_D() << "dossier beyond:"
                  << beyond;
    }

    QStringList description_filters;
    if        (qsettings.contains(  "description_filter")) {
      parse_singlex(qsettings,   "description_filter",   description_filters);
    } else if (subgroups.contains(  "description_filter")) {
      parse_several(qsettings,   "description_filter",   description_filters);
    }

    for (const QString& filter_str : qAsConst(description_filters)) {
      LVD_LOG_D() << "dossier description filter:"
                  << filter_str;

      QRegularExpression filter(filter_str);
      dossier.  description_filters.append(filter);
    }

    QStringList fingerprint_filters;
    if        (qsettings.contains(  "fingerprint_filter")) {
      parse_singlex(qsettings,   "fingerprint_filter",   fingerprint_filters);
    } else if (subgroups.contains(  "fingerprint_filter")) {
      parse_several(qsettings,   "fingerprint_filter",   fingerprint_filters);
    }

    for (const QString& filter_str : qAsConst(fingerprint_filters)) {
      LVD_LOG_D() << "dossier fingerprint filter:"
                  << filter_str;

      QRegularExpression filter(filter_str);
      dossier.  fingerprint_filters.append(filter);
    }

    QStringList location_path_filters;
    if        (qsettings.contains("location_path_filter")) {
      parse_singlex(qsettings, "location_path_filter", location_path_filters);
    } else if (subgroups.contains("location_path_filter")) {
      parse_several(qsettings, "location_path_filter", location_path_filters);
    }

    for (const QString& filter_str : qAsConst(location_path_filters)) {
      LVD_LOG_D() << "dossier location path filter:"
                  << filter_str;

      QRegularExpression filter(filter_str);
      dossier.location_path_filters.append(filter);
    }

    QStringList location_line_filters;
    if        (qsettings.contains("location_line_filter")) {
      parse_singlex(qsettings, "location_line_filter", location_line_filters);
    } else if (subgroups.contains("location_line_filter")) {
      parse_several(qsettings, "location_line_filter", location_line_filters);
    }

    for (const QString& filter_str : qAsConst(location_line_filters)) {
      LVD_LOG_D() << "dossier location line filter:"
                  << filter_str;

      QRegularExpression filter(filter_str);
      dossier.location_line_filters.append(filter);
    }

    for (QString& key : qsettings.childKeys()  ) {
      if (key.endsWith("_analyzer")) {
        LVD_LOG_D() << "dossier analyzer:"
                    << key;

        Analyzer* analyzer = Analyzer::create(key, this);
        Analyzer::connect(analyzer, &Analyzer::report,
                          this, &Reports::on_report);

        Analyzer::connect(analyzer, &Analyzer::depend,
                          this, &Reports::on_depend);

        dossier.analyzers.append(analyzer);
      }
    }

    for (QString& key : qsettings.childGroups()) {
      if (key.endsWith("_analyzer")) {
        LVD_LOG_D() << "dossier analyzer:"
                    << key;

        Analyzer* analyzer = Analyzer::create(key, this);
        Analyzer::connect(analyzer, &Analyzer::report,
                          this, &Reports::on_report);

        dossier.analyzers.append(analyzer);
      }
    }

    dossiers_.append(dossier);
  }

  earlier_path_ = QFileInfo(QDir::currentPath()).absoluteFilePath();
  return true;
}

bool Reports::state_execute_reports() {
  LVD_LOG_T();

  if (dossiers_.isEmpty()) {
    return true;
  }

  if (dossier_ == nullptr) {
    dossier_  = &dossiers_.first();

    if (!dossier_->path.isEmpty()) {
      desired_path_ = QFileInfo(dossier_->path).absoluteFilePath();
    }
  }

  if (!dossier_->before.isEmpty()) {
    QString command = dossier_->before.takeFirst();

    LVD_LOG_D() << "before command"
                << command;

    state_execute_reports_impl(command, false);

    push_state(&Reports::state_execute_reports);
    return false;
  }

  if (!dossier_->during.isEmpty()) {
    QString command = dossier_->during.takeFirst();

    LVD_LOG_D() << "during command"
                << command;

    state_execute_reports_impl(command, true);

    push_state(&Reports::state_execute_reports);
    return false;
  }

  if (!dossier_->beyond.isEmpty()) {
    QString command = dossier_->beyond.takeFirst();

    LVD_LOG_D() << "beyond command"
                << command;

    state_execute_reports_impl(command, false);

    push_state(&Reports::state_execute_reports);
    return false;
  }

  if (!current_path_.isEmpty()) {
    LVD_LOG_D() << "change working directory to"
                << earlier_path_;

    bool ok = QDir::setCurrent(earlier_path_);
    if (!ok) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "cannot change working directory to"
                          << earlier_path_;

      emit failure(message);
      return false;
    }

    current_path_.clear();
  }

  if (!desired_path_.isEmpty() && !dossier_->keep_path) {
    LVD_LOG_D() << "remove working directory in"
                << desired_path_;

    bool ok = QDir(desired_path_).removeRecursively();
    if (!ok) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "cannot remove working directory in"
                          << desired_path_;

      emit failure(message);
      return false;
    }

    desired_path_.clear();
  }

  if (dossier_ != nullptr || true) {
    dossier_  = nullptr;
    dossiers_.pop_front();
  }

  push_state(&Reports::state_execute_reports);
  return true;
}

void Reports::state_execute_reports_impl(const QString& command,
                                         const bool     analyze) {
  LVD_LOG_T();

  if (!desired_path_.isEmpty()) {
    if (!current_path_.isEmpty()) {
      if ( current_path_ != desired_path_) {
        LVD_LOG_D() << "change working directory to"
                    << earlier_path_;

        bool ok = QDir::setCurrent(earlier_path_);
        if (!ok) {
          LVD_LOGBUF message;
          LVD_LOG_W(&message) << "cannot change working directory to"
                              << earlier_path_;

          emit failure(message);
          return;
        }

        current_path_.clear();
      }
    }

    if ( current_path_.isEmpty()) {
      if (!QDir(desired_path_).exists()) {
        LVD_LOG_D() << "create working directory in"
                    << desired_path_;

        QDir(desired_path_).mkpath(".");
      }

      LVD_LOG_D() << "change working directory to"
                  << desired_path_;

      bool ok = QDir::setCurrent(desired_path_);
      if (!ok) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << "cannot change working directory to"
                            << earlier_path_;

        emit failure(message);
        return;
      }

      current_path_ = desired_path_;
    }
  }

  Q_ASSERT(executor_ == nullptr);
  executor_ = new Executor(this);

  { connect(executor_, &Executor::success,
            this, &Reports::on_success);

    connect(executor_, &Executor::failure,
            this, &Reports::on_failure); }

  if (analyze) {
    connect(executor_, &Executor::analyze,
            this, &Reports::on_analyze);
  }

  int hard_timeout = dossier_->hard_timeout < 0
      ? hard_timeout_ : dossier_->hard_timeout;

  if (hard_timeout > 0) {
    executor_->set_hard_timeout(hard_timeout);
  }

  int soft_timeout = dossier_->soft_timeout < 0
      ? soft_timeout_ : dossier_->soft_timeout;

  if (soft_timeout > 0) {
    executor_->set_soft_timeout(soft_timeout);
  }

  executor_->execute(command);
}

bool Reports::state_combine_reports() {
  LVD_LOG_T();

  Report::s::iterator last;

  LVD_LOG_D() << reports_.size()
              << "messages before remove";

  last =
  std::remove_if(reports_.begin(),
                 reports_.end(),
  [&] (const Report& report) {
    return !report.location_path().startsWith(source_);
  });

  reports_.erase(last, reports_.end());

  LVD_LOG_D() << reports_.size()
              << "messages after remove";

  QCollator qcollator(QLocale::English);
  qcollator.setNumericMode(true);

  std::sort  (reports_.begin(),
              reports_.  end(),
  [qcollator] (const Report& lhs, const Report& rhs) {
    if (lhs.location_path() < rhs.location_path())
      return true;
    if (lhs.location_path() > rhs.location_path())
      return false;

    if (qcollator.compare(lhs.location_line(), rhs.location_line()) < 0)
      return true;
    if (qcollator.compare(lhs.location_line(), rhs.location_line()) > 0)
      return false;

    return lhs.fingerprint() <  rhs.fingerprint();
  });

  LVD_LOG_D() << reports_.size()
              << "messages before unique";

  last =
  std::unique(reports_.begin(),
              reports_.  end(),
  [] (const Report& lhs, const Report& rhs) {
    return lhs.fingerprint() == rhs.fingerprint();
  });

  reports_.erase(last, reports_.end());

  LVD_LOG_D() << reports_.size()
              << "messages after unique";

  return true;
}

bool Reports::state_combine_depends() {
  LVD_LOG_T();

  Depend::s::iterator last;

  QCollator qcollator(QLocale::English);
  qcollator.setNumericMode(true);

  std::sort  (depends_.begin(),
              depends_.  end(),
  [qcollator] (const Depend& lhs, const Depend& rhs) {
    if (lhs.package() < rhs.package()) {
      return true;
    }
    if (lhs.package() > rhs.package()) {
      return false;
    }

    if (lhs.version() < rhs.version()) {
      return true;
    }
    if (lhs.version() > rhs.version()) {
      return false;
    }

    return false;
  });

  LVD_LOG_D() << depends_.size()
              << "depends before unique";

  last =
  std::unique(depends_.begin(),
              depends_.end(),
              [] (const Depend& lhs, const Depend& rhs) {
    return lhs.package() == rhs.package()
        && lhs.version() == rhs.version();
  });

  depends_.erase(last, depends_.end());

  LVD_LOG_D() << depends_.size()
              << "depends after unique";

  return true;
}

bool Reports::state_publish_reports() {
  LVD_LOG_T();

  if (reports_path_.isEmpty()) {
    LVD_LOG_D() << "reports path not configured";
    return true;
  }

  QJsonArray qjsonarray;

  for (const Report& report : qAsConst(reports_)) {
    Q_ASSERT(report.valid());

    QJsonObject qjsonobject;

    LVD_FINALLY {
      qjsonarray.append(qjsonobject);
    };

    QJsonValue description = report.description().trimmed();
    qjsonobject.insert("description", description);

    QJsonValue fingerprint = report.fingerprint().trimmed();
    qjsonobject.insert("fingerprint", fingerprint);

    QJsonObject location;

    LVD_FINALLY {
      qjsonobject.insert("location", location);
    };

    QString relative_path = report.relative_path();
    location.insert("path" , relative_path);

    QJsonObject lwrapper;

    LVD_FINALLY {
      location   .insert("lines"   , lwrapper);
    };

    bool ok;

    QJsonValue location_line = report.location_line().toInt(&ok);
    lwrapper.insert("begin", location_line);

    Q_ASSERT(ok);
  }

  QJsonDocument qjsondocument;
  qjsondocument.setArray(qjsonarray);

  QByteArray data = qjsondocument.toJson();

  LVD_LOG_D() << "save reports to"
              << reports_path_;

  QFile qfile(reports_path_);
  qfile.open(QFile::WriteOnly);

  if (!qfile.isOpen()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "cannot open reports file"
                        << reports_path_;

    emit failure(message);
    return false;
  }

  qfile.write(data);
  return true;
}

bool Reports::state_publish_depends() {
  LVD_LOG_T();

  if (depends_path_.isEmpty()) {
    LVD_LOG_D() << "depends path not configured";
    return true;
  }

  QJsonArray qja_deps;
  QJsonArray qja_vuln;

  for (const Depend& depend : qAsConst(depends_)) {
    QJsonObject qjo_pack;
    qjo_pack.insert("name"   , depend.package());

    QJsonObject qjo_depe;
    qjo_depe.insert("package",        qjo_pack  );
    qjo_depe.insert("version", depend.version());

    qja_deps.append(qjo_depe);

    Vulnerabilities vulnerabilities = depend.vulnerabilities();

    for (const Vulnerability& vulnerability : qAsConst(vulnerabilities)) {
      QJsonObject qjo_vuln;

      qjo_vuln.insert("category", "dependency_scanning");
      qjo_vuln.insert("message" , vulnerability.cveident());
      qjo_vuln.insert("cve"     , vulnerability.cveident());
      qjo_vuln.insert("severity", vulnerability.severity().to_string());

      QJsonObject qjo_scan;
      qjo_scan.insert("id"  , "reports");
      qjo_scan.insert("name", "Reports");

      qjo_vuln.insert("scanner", qjo_scan);

      QJsonObject qjo_loca;
      qjo_loca.insert("file"      , "README.md");
      qjo_loca.insert("dependency", qjo_depe);
      qjo_vuln.insert("location"  , qjo_loca);

      QJsonObject qjo_iden;
      qjo_iden.insert("type" , "cve");
      qjo_iden.insert("name" , vulnerability.cveident());
      qjo_iden.insert("value", vulnerability.cveident());
      qjo_iden.insert("url"  , "https://cve.mitre.org/cgi-bin/cvename.cgi?name=" + vulnerability.cveident());

      QJsonArray qja_iden;
      qja_iden.append(qjo_iden);

      qjo_vuln.insert("identifiers", qja_iden);

      qja_vuln.append(qjo_vuln);
    }
  }

  QJsonObject qjo_defi;
  qjo_defi.insert("path"           , "README.md");
  qjo_defi.insert("package_manager", "Reports");
  qjo_defi.insert("dependencies", qja_deps);

  QJsonArray  qja_defi;
  qja_defi.append(qjo_defi);

  QJsonObject qjsonobject;
  qjsonobject.insert("version"        ,  "2.1" );
  qjsonobject.insert("vulnerabilities", qja_vuln);

  qjsonobject.insert("dependency_files", qja_defi);

  QJsonDocument qjsondocument;
  qjsondocument.setObject(qjsonobject);

  QByteArray data = qjsondocument.toJson();

  LVD_LOG_D() << "save depends to"
              << depends_path_;

  QFile qfile(depends_path_);
  qfile.open(QFile::WriteOnly);

  if (!qfile.isOpen()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "cannot open depends file"
                        << depends_path_;

    emit failure(message);
    return false;
  }

  qfile.write(data);
  return true;
}

bool Reports::state_finished() {
  LVD_LOG_T();

  emit success();
  return true;
}

// ----------

void Reports::on_success(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  if (executor_) {
    executor_->deleteLater();
    executor_ = nullptr;
  }

  QMetaObject::invokeMethod(this, [&] {
    LVD_LOG_T();
    LVD_SHIELD;

//    clear_states(); NO!
    exec_state();

    LVD_SHIELD_FUN(&Reports::failure);
  }, Qt::QueuedConnection);

  LVD_SHIELD_FUN(&Reports::failure);
}

void Reports::on_failure(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  if (executor_) {
    executor_->deleteLater();
    executor_ = nullptr;
  }

  clear_states();
  emit failure(message);

  LVD_SHIELD_FUN(&Reports::failure);
}

void Reports::on_analyze(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  if (dossier_) {
    for (Analyzer* analyzer : qAsConst(dossier_->analyzers)) {
      analyzer->analyze(message);
    }
  }

  LVD_SHIELD_FUN(&Reports::failure);
}

void Reports::on_report(      Report  report) {
  LVD_LOG_T() << LVD_LOGLINE(report.description());
  LVD_SHIELD;

  if (dossier_) {
    if (!dossier_->description_filters.isEmpty()) {
      LVD_LOG_D() << "filter by description filters";

      for (const QRegularExpression& filter : qAsConst(dossier_->description_filters)) {
        auto match = filter.match(report.description());
        if (match.hasMatch()) {
          LVD_LOG_D() << "message"
                      << LVD_LOGLINE(report.description())
                      << LVD_LOGLINE(report.description())
                      << "filtered by description filter"
                      << filter.pattern();

          return;
        }
      }
    }

    if (!dossier_->fingerprint_filters.isEmpty()) {
      LVD_LOG_D() << "filter by fingerprint filters";

      for (const QRegularExpression& filter : qAsConst(dossier_->fingerprint_filters)) {
        auto match = filter.match(report.fingerprint());
        if (match.hasMatch()) {
          LVD_LOG_D() << "message"
                      << LVD_LOGLINE(report.description())
                      << LVD_LOGLINE(report.fingerprint())
                      << "filtered by fingerprint filter"
                      << filter.pattern();

          return;
        }
      }
    }

    QString relative_path = report.location_path();
    if (relative_path.startsWith(source_)) {
      relative_path = lstrip(relative_path.midRef(source_.size()), "/").toString();
      report.set_relative_path(relative_path);
    } else
    if (relative_path.startsWith(binary_)) {
      relative_path = lstrip(relative_path.midRef(binary_.size()), "/").toString();
      report.set_relative_path(relative_path);
    } else {
      return;
    }

    LVD_LOG_D() << "set relative path"
                << relative_path;

    if (!dossier_->location_path_filters.isEmpty()) {
      LVD_LOG_T() << "filter by location path filters";

      for (const QRegularExpression& filter : qAsConst(dossier_->location_path_filters)) {
        auto match = filter.match(report.relative_path());
        if (match.hasMatch()) {
          LVD_LOG_D() << "message"
                      << LVD_LOGLINE(report.description())
                      << LVD_LOGLINE(report.relative_path())
                      << "filtered by location path filter"
                      << filter.pattern();

          return;
        }
      }
    }

    if (!dossier_->location_line_filters.isEmpty()) {
      LVD_LOG_D() << "filter by location line filters";

      for (const QRegularExpression& filter : qAsConst(dossier_->location_line_filters)) {
        auto match = filter.match(report.location_line());
        if (match.hasMatch()) {
          LVD_LOG_D() << "message"
                      << LVD_LOGLINE(report.description())
                      << LVD_LOGLINE(report.location_line())
                      << "filtered by location line filter"
                      << filter.pattern();

          return;
        }
      }
    }
  }

  reports_.append(report);

  LVD_SHIELD_FUN(&Reports::failure);
}

void Reports::on_depend(const Depend& depend) {
  LVD_LOG_T() << depend.package()
              << depend.version();

  LVD_SHIELD;

  depends_.append(depend);

  LVD_SHIELD_FUN(&Reports::failure);
}

}  // namespace lvd::reports

#include "lvd/core_task.hxx"
