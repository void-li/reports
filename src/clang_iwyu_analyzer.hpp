/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "lvd/core.hpp"
#include "lvd/core_enum.hpp"

#include <QObject>
#include <QRegularExpression>
#include <QString>

#include "lvd/logger.hpp"

#include "analyzer.hpp"

// ----------

namespace lvd::reports {

class ClangIwyuAnalyzer : public Analyzer {
  Q_OBJECT LVD_LOGGER

 private:
  LVD_ENUM(State,
           Search,
           Create,
           Remove);

 public:
  ClangIwyuAnalyzer        (QObject* parent = nullptr);
  ~ClangIwyuAnalyzer() override;

  static
  ClangIwyuAnalyzer* create(QObject* parent = nullptr) {
    return new ClangIwyuAnalyzer(parent);
  }

 private:
  void analyze_impl(const QString& line) override;

 private:
  State state_ = State::Search;

 private:
  QString description_;
  QString fingerprint_;

  QString location_path_;
  QString location_line_;

 private:
  QRegularExpression search_create_expression_;
  QRegularExpression search_remove_expression_;

  QRegularExpression inside_matchi_expression_;
  QRegularExpression inside_matchc_expression_;
};

DECLARE_ANALYZER(ClangIwyuAnalyzer, clang_iwyu_analyzer);

}  // namespace lvd::reports
