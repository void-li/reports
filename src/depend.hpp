/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QString>
#include <QVector>

#include "lvd/core_enum.hpp"
#include "lvd/metatype.hpp"

#include "vulnerability.hpp"

// ----------

namespace lvd::reports {

class Depend {
 public:
  using s = QVector<Depend>;

 public:
  Depend() {}

  Depend(const QString&         package,
         const QString&         version,
         const Vulnerabilities& vulnerabilities)
      : package_(package),
        version_(version),
        vulnerabilities_(vulnerabilities) {}

 public:
  QString package() const {
    return package_;
  }

  QString version() const {
    return version_;
  }

  Vulnerabilities vulnerabilities() const {
    return vulnerabilities_;
  }

 private:
  QString package_;
  QString version_;

  Vulnerabilities vulnerabilities_;
};

}  // namespace lvd::reports

Q_DECLARE_METATYPE(lvd::reports::Depend)
L_DECLARE_METATYPE(lvd::reports::Depend, Depend);
