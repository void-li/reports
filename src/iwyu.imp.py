#!/usr/bin/python
#
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import os
import re

import sys

# config

qmodules = [
  'QtCore',
  'QtGui',
  'QtNetwork',
  'QtQml',
  'QtQuick',
  'QtTest'
]

qsymbol_rx = re.compile('^Q[A-Z][A-Za-z]+$')
include_rx = re.compile('^#include "([a-z]+\.h)"$')

sym_db = {}
inc_db = {}

# source

for root, rdirs, _ in os.walk('/usr/include/qt'):
  for rdir in rdirs:
    if rdir not in qmodules:
      continue

    if rdir not in sym_db:
      sym_db[rdir] = []

    if rdir not in inc_db:
      inc_db[rdir] = {}

    for base, _, qsyms in os.walk('/usr/include/qt/' + rdir):
      for qsym in qsyms:
        match = qsymbol_rx.match(qsym)
        if not match:
          continue

        with open('/usr/include/qt/' + rdir + '/' + qsym) as f:
          data = f.read().strip()

        match = include_rx.match(data)
        if not match:
          continue

        inc = match.group(1)

        if inc not in inc_db[rdir]:
          inc_db[rdir][inc] = []

        inc_db[rdir][inc].append(qsym)
        sym_db[rdir]     .append(qsym)

      break

  break

#

size = 0

for k, v in sym_db.items():
  for s in v:
    if len(s) > size:
      size = len(s)

for k, _ in inc_db.items():
  if len(k) > size - 2:
    size = len(k) + 2

# ----------

def custom(filename):
  global spaced
  if not spaced:
    print()
  spaced = False

  print('  # Custom   {} #'.format('-' * (2 * size + 34)))
  print()

  with open(filename) as f:
    for line in f.readlines():
      line = line.strip()

      if not line:
        continue

      if   line.startswith('s'):
        a, x, b, y, c = [ x.strip() for x in line.split(' ') if x]

        if '"' in c:
          c = c.replace('"', '\\"')
        else:
          c = '<' + c + '>'

        print('  {{ symbol : [ "{}"{}, "{}"{}, "{}"{}, "{}"{} ] }},'
              .format(b, ' ' * (size - len(b) - 0),
                      x, ' ' * (x == 'public'),
                      c, ' ' * (size - len(c) + 2),
                      y, ' ' * (y == 'public')))

      elif line.startswith('i'):
        a, x, b, y, c = [ x.strip() for x in line.split(' ') if x]

        if '"' in c:
          c = c.replace('"', '\\"')
        else:
          c = '<' + c + '>'

        print('  {{ include: [ "{}"{}, "{}"{}, "{}"{}, "{}"{} ] }},'
              .format(b, ' ' * (size - len(b) - 0),
                      x, ' ' * (x == 'public'),
                      c, ' ' * (size - len(c) + 2),
                      y, ' ' * (y == 'public')))

      elif line.startswith('#'):
        print()

# ----------

def symbols():
  global spaced
  if not spaced:
    print()
  spaced = False

  print('  # Symbols  {} #'.format('-' * (2 * size + 34)))

  for k, v in sym_db.items():
    print()
    print('  # {}'.format(k))

    v.sort()

    for s in v:
      print('  {{ symbol : [ "{}"{}, "private", "<{}>"{}, "public" ] }},'
            .format(s, ' ' * (size - len(s) - 0), s, ' ' * (size - len(s))))

# ----------

def includes():
  global spaced
  if not spaced:
    print()
  spaced = False

  print('  # Includes {} #'.format('-' * (2 * size + 34)))

  for k, v in inc_db.items():
    print()
    print('  # {}'.format(k))

    w = list(v.keys())
    w.sort()

    for x in w:
      y = v[x]
      y.sort()

      for i in y:
        print('  {{ include: [ "<{}>"{}, "private", "<{}>"{}, "public" ] }},'
              .format(x, ' ' * (size - len(x) - 2), i, ' ' * (size - len(i))))

# ----------

spaced = True

if __name__ == '__main__':
  print('[')

  custom('iwyu.imp.pref.txt')

  symbols()
  includes()

  custom('iwyu.imp.suff.txt')

  print(']')
