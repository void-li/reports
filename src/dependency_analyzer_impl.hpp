/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <functional>

#include <QHash>
#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "depend.hpp"

// ----------

namespace lvd::reports {

class DependencyAnalyzerImpl : public QObject {
  Q_OBJECT LVD_LOGGER

 private:
  using  Declaration  =                std::function<DependencyAnalyzerImpl*(QObject*)> ;
  using  Declarations = QHash<QString, std::function<DependencyAnalyzerImpl*(QObject*)>>;

  static Declarations& declarations();

 public:
  template <class T>
  static bool declare(const QString& species) {
    declarations()[species] = &T::create;
    return true;
  }

 protected:
  DependencyAnalyzerImpl        (QObject* parent = nullptr);

 public:
  ~DependencyAnalyzerImpl() override;

  static
  DependencyAnalyzerImpl* create(QObject* parent = nullptr);

 public:
  Depend::s analyze     (const QString& path) {
    return analyze_impl(path);
  }

 protected:
  virtual
  Depend::s analyze_impl(const QString& path) = 0;
};

#ifndef DECLARE_DEPENDENCY_ANALYZER_IMPL
#define DECLARE_DEPENDENCY_ANALYZER_IMPL(Type, Name) \
  inline const bool Name ## __ ## Decl = lvd::reports::DependencyAnalyzerImpl::declare<Type>(#Name);
#endif  // DECLARE_DEPENDENCY_ANALYZER_IMPL

}  // namespace lvd::reports
