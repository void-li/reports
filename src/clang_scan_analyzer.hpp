/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QRegularExpression>
#include <QString>

#include "lvd/logger.hpp"

#include "analyzer.hpp"

// ----------

namespace lvd::reports {

class ClangScanAnalyzer : public Analyzer {
  Q_OBJECT LVD_LOGGER

 public:
  ClangScanAnalyzer        (QObject* parent = nullptr);
  ~ClangScanAnalyzer() override;

  static
  ClangScanAnalyzer* create(QObject* parent = nullptr) {
    return new ClangScanAnalyzer(parent);
  }

 private:
  void analyze_impl(const QString& line) override;

 private:
  QRegularExpression search_expression_;
};

DECLARE_ANALYZER(ClangScanAnalyzer, clang_scan_analyzer);

}  // namespace lvd::reports
