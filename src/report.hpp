/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QMetaType>
#include <QString>
#include <QVector>

#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"

// ----------

namespace lvd::reports {

class Report {
  LVD_LOGGER

 public:
  using s = QVector<Report>;

 public:
  Report();

  Report(const QString& description,
         const QString& fingerprint,
         const QString& location_path,
         const QString& location_line);

 public:
  bool valid() const {
    return valid_;
  }

 public:
  QString description() const {
    return description_;
  }
  QString fingerprint() const {
    return fingerprint_;
  }

  QString location_path() const {
    return location_path_;
  }
  QString location_line() const {
    return location_line_;
  }

  QString relative_path() const {
    return relative_path_;
  }
  void set_relative_path(const QString& relative_path) {
    relative_path_ = relative_path;
  }

 private:
  bool valid_ = false;

  QString description_;
  QString fingerprint_;

  QString location_path_;
  QString location_line_;

  QString relative_path_;
};

}  // namespace lvd::reports

Q_DECLARE_METATYPE(lvd::reports::Report);
L_DECLARE_METATYPE(lvd::reports::Report, Report);
