/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "lvd/core.hpp"  // IWYU pragma: keep
#include "lvd/core_task.hpp"

#include <QList>
#include <QObject>
#include <QRegularExpression>
#include <QString>
#include <QStringList>
#include <QVector>

#include "lvd/logger.hpp"

#include "analyzer.hpp"
#include "depend.hpp"
#include "report.hpp"

// ----------

namespace lvd::reports {

class Reports : public QObject, private Task<Reports> {
  Q_OBJECT LVD_LOGGER

 private:
  struct Dossier;
  using  Dossiers = QVector<Dossier>;

  struct Dossier {
    QString name;
    QString  path;
    bool keep_path = true;

    int hard_timeout = -1;
    int soft_timeout = -1;

    QStringList before;
    QStringList during;
    QStringList beyond;

    using Filters = QVector<QRegularExpression>;

    Filters   description_filters;
    Filters   fingerprint_filters;

    Filters location_path_filters;
    Filters location_line_filters;

    QVector<Analyzer*> analyzers;
  };

 private:
  class Executor;

 public:
  Reports(const QString& config,
          QObject*       parent = nullptr);
  ~Reports() override;

 public slots:
  void execute();

 signals:
  void success(const QString& message = "");
  void failure(const QString& message = "");

 private:
  bool state_settings();

  bool state_execute_reports();
  void state_execute_reports_impl(const QString& command,
                                  const bool     analyze);

  bool state_combine_reports();
  bool state_combine_depends();

  bool state_publish_reports();
  bool state_publish_depends();

  bool state_finished();

 private slots:
  void on_success(const QString& message = "");
  void on_failure(const QString& message = "");

  void on_analyze(const QString& message);

  void on_report(      Report  report);
  void on_depend(const Depend& depend);

 private:
  QString config_;

 private:
  QString reports_path_;
  QString depends_path_;

  QString source_ = ".";
  QString binary_ = ".";

  int hard_timeout_ = -1;
  int soft_timeout_ = -1;

 private:
  Dossiers dossiers_;
  Dossier* dossier_ = nullptr;

  Report::s reports_;
  Depend::s depends_;

 private:
  Executor* executor_ = nullptr;

 private:
  QString earlier_path_;
  QString current_path_;
  QString desired_path_;
};

}  // namespace lvd::reports
