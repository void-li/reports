/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "report.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QCryptographicHash>

// ----------

namespace lvd::reports {

Report::Report()
    : valid_(false) {}

Report::Report(const QString& description,
               const QString& fingerprint,
               const QString& location_path,
               const QString& location_line)
    : valid_(true ),
      description_  (description),
      fingerprint_  (fingerprint),
      location_path_(location_path),
      location_line_(location_line) {
  if (description_.isEmpty()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "missing message description";

    LVD_THROW_RUNTIME(message);
  }

  if (fingerprint_.isEmpty()) {
    QCryptographicHash qcryptographichash(QCryptographicHash::Keccak_256);
    qcryptographichash.addData(description_  .toUtf8());

    qcryptographichash.addData(location_path_.toUtf8());
    qcryptographichash.addData(location_line_.toUtf8());

    fingerprint_ = qcryptographichash.result().toBase64();
  }

  if (location_path_.isEmpty()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "missing message location path";

    LVD_THROW_RUNTIME(message);
  }

  if (location_line_.isEmpty()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "missing message location line";

    LVD_THROW_RUNTIME(message);
  }
}

}  // namespace lvd::reports
