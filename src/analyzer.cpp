/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "analyzer.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

// ----------

namespace lvd::reports {

Analyzer::Declarations& Analyzer::declarations() {
  static Declarations declarations;
  return declarations;
}

// ----------

Analyzer::Analyzer        (QObject*       parent)
    : QObject(parent) {
  LVD_LOG_T();
}

Analyzer::~Analyzer() {
  LVD_LOG_T();
}

// ----------

Analyzer* Analyzer::create(const QString& species,
                           QObject*       parent) {
  LVD_LOG_T() << species;

  auto& declarations = Analyzer::declarations();
  if (!declarations.contains(species)) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "unknown analyzer"
                        << species;

    LVD_THROW_RUNTIME(message);
  }

  return declarations[species](parent);
}

}  // namespace lvd::reports
