/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "clang_lazy_analyzer.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QFileInfo>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "report.hpp"

// ----------

namespace lvd::reports {

ClangLazyAnalyzer::ClangLazyAnalyzer(QObject* parent)
    : Analyzer(parent) {
  LVD_LOG_T();

  search_expression_.setPattern(
    "^(?<path>[^:]+):(?<line>[0-9]+):([0-9]+): ([^:]+): (?<message>.+)\\[(?<warning>.+)\\]$"
  ); Q_ASSERT(search_expression_.isValid());
}

ClangLazyAnalyzer::~ClangLazyAnalyzer() {
  LVD_LOG_T();
}

// ----------

void ClangLazyAnalyzer::analyze_impl(const QString& line) {
  LVD_LOG_T() << line;

  auto match = search_expression_.match(line);
  if (match.hasMatch()) {
    QString description   = QString("%1[%2]").arg(match.captured("message"),
                                                  match.captured("warning"));

    QString location_path = match.captured("path");
    QString location_line = match.captured("line");

    if (location_line.isEmpty()) {
      location_line = "0";
    }

    const QFileInfo qfileinfo(location_path);
    location_path = qfileinfo.absoluteFilePath();

    LVD_LOG_D() << "new message"
                << LVD_LOGLINE(description);

    Report message(description,
                   QString(),
                   location_path,
                   location_line);

    emit this->report(message);
  }
}

}  // namespace lvd::reports
