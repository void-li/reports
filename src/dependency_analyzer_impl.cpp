/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "dependency_analyzer_impl.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QFile>
#include <QIODevice>
#include <QStringRef>

#include "lvd/string.hpp"

// ----------

namespace lvd::reports {

DependencyAnalyzerImpl::Declarations& DependencyAnalyzerImpl::declarations() {
  static Declarations declarations;
  return declarations;
}

// ----------

DependencyAnalyzerImpl::DependencyAnalyzerImpl        (QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();
}

DependencyAnalyzerImpl::~DependencyAnalyzerImpl() {
  LVD_LOG_T();
}

// ----------

DependencyAnalyzerImpl* DependencyAnalyzerImpl::create(QObject* parent) {
  LVD_LOG_T();

  for (const QString& path : { "/etc/"  "os-release",
                               "/usr/lib/os-release" }) {
    QFile qfile(path);
    qfile.open(QFile::ReadOnly);

    if (qfile.isOpen()) {
      LVD_LOG_D() << "searching dependency analyzer in"
                  << path;

      while (!qfile.atEnd()) {
        QByteArray line = qfile.readLine();

        static const QByteArray PREFIX = "ID=";

        if (line.startsWith(PREFIX)) {
          QString species = line.mid(PREFIX.length());
          species = strip(species, " \r\n\t\"").toString();

          auto& declarations = DependencyAnalyzerImpl::declarations();
          if (!declarations.contains(species)) {
            LVD_LOG_D() << "unknown dependency analyzer:"
                        << species;

            continue;
          }

          return declarations[species](parent);
        }
      }
    }
  }

  return nullptr;

}

}  // namespace lvd::reports
