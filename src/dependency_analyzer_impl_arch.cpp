/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "dependency_analyzer_impl_arch.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QByteArray>
#include <QList>
#include <QProcess>
#include <QRegularExpressionMatch>
#include <QStringList>
#include <QThread>
#include <QVector>

#include "lvd/string.hpp"

// ----------

namespace lvd::reports {

DependencyAnalyzerImplArch::DependencyAnalyzerImplArch(QObject* parent)
    : DependencyAnalyzerImpl(parent) {
  LVD_LOG_T();

  owned_expression_.setPattern(
    "is owned by (?<package>[^ ]+) ([0-9]+:|)(?<version>[^ ]+)-[0-9]+$"
  );

  audit_expression_.setPattern(
    "Package (?<package>.+) is affected by (?<cvenums>((, |)CVE-[0-9]{4}-[0-9]+)+). (?<grading>.+) risk!"
  );

  QProcess qprocess;
  qprocess.start(QString("arch-audit"), QStringList());
  qprocess.waitForStarted();

  while (qprocess.state() == QProcess::Running || !qprocess.atEnd()) {
    qprocess.waitForReadyRead();
    bool readLine = false;

    while (qprocess.canReadLine()) {
      QByteArray line = strip(qprocess.readLine());
      readLine = true;

      LVD_LOG_D() << "parsing line:"
                  << line;

      auto match = audit_expression_.match(line);
      if (match.hasMatch()) {
        QString package = match.captured("package");
        QString cvenums = match.captured("cvenums");

        QString grading = match.captured("grading");
        Vulnerabilities vulnerabilities;

        Vulnerability::Severity severity;

        try {
          severity = Vulnerability::Severity::From_String(grading.toLocal8Bit().data());
        } catch (const std::exception&) {
          severity = Vulnerability::Severity::Unknown;
        }

        QStringList cvenums_list = cvenums.split(", ");

        for (const QString& failure : cvenums_list) {
          Vulnerability vulnerability(failure, severity);
          vulnerabilities.push_back(vulnerability);
        }

        vulnerabilities_[package] = vulnerabilities;
      }
      else {
        LVD_LOG_D() << "invalid line:"
                    << line;
      }
    }

    if (!readLine) {
      QThread::msleep(1);
    }
  }
}

DependencyAnalyzerImplArch::~DependencyAnalyzerImplArch() {
  LVD_LOG_T();
}

// ----------

Depend::s DependencyAnalyzerImplArch::analyze_impl(const QString& path) {
  LVD_LOG_T() << path;

  Depend::s depends;

  QProcess qprocess;
  qprocess.start("pacman", { "-Qo", path });
  qprocess.waitForStarted();

  while (qprocess.state() == QProcess::Running || !qprocess.atEnd()) {
    qprocess.waitForReadyRead();
    bool readLine = false;

    while (qprocess.canReadLine()) {
      QByteArray line = strip(qprocess.readLine());
      readLine = true;

      LVD_LOG_D() << "parsing line:"
                  << line;

      auto match = owned_expression_.match(line);
      if (match.hasMatch()) {
        QString package = match.captured("package");
        QString version = match.captured("version");

        Vulnerabilities vulnerabilities = vulnerabilities_.value(package, {});

        Depend depend(package, version, vulnerabilities);
        depends.push_back(depend);
      }
      else {
        LVD_LOG_W() << "invalid line:"
                    << line;
      }
    }

    if (!readLine) {
      QThread::msleep(1);
    }
  }

  return depends;
}

}  // namespace lvd::reports
