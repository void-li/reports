/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QProcess>
#include <QString>
#include <QTimer>

#include "lvd/logger.hpp"

#include "reports.hpp"

// ----------

namespace lvd::reports {

class Reports::Executor : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  Executor(QObject* parent = nullptr);
  ~Executor() override;

 public slots:
  void execute(const QString& command);

 public:
  int hard_timeout() const { return hard_timeout_; }
  void set_hard_timeout(int hard_timeout) {
    hard_timeout_ = hard_timeout;
  }

  int soft_timeout() const { return soft_timeout_; }
  void set_soft_timeout(int soft_timeout) {
    soft_timeout_ = soft_timeout;
  }

 signals:
  void success(const QString& message = "");
  void failure(const QString& message = "");

  void analyze(const QString& message);

 private:
  void create_process();
  void remove_process();

  void create_hard_timer();
  void remove_hard_timer();

  void create_soft_timer();
  void remove_soft_timer();

  void notify_soft_timer();
  void ignore_soft_timer();

 private slots:
  void on_finished();
  void on_failure(QProcess::ProcessError process_error);

  void on_read_stdout();
  void on_read_stderr();

  void on_hard_timeout();
  void on_soft_timeout();

 private:
  QProcess* qprocess_ = nullptr;

 private:
  QTimer* hard_timer_   = nullptr;
  int     hard_timeout_ = -1;

  QTimer* soft_timer_   = nullptr;
  int     soft_timeout_ = -1;
};

}  // namespace lvd::reports
