/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>
#include <exception>

#include <QCommandLineParser>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QString>
#include <QStringList>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"

#include "config.hpp"
#include "reports.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::reports::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::reports::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::reports::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::reports::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
  [] (const QString& message) {
    if (!message.isEmpty()) {
      lvd::qStdErr() << message << Qt::endl;
    }

    lvd::Application::exit(1);
  });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  qcommandlineparser.addPositionalArgument("config",
                                           "Config file.",
                                           "[<config>]");

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  QStringList arguments = qcommandlineparser.positionalArguments();

  QString config = ".li.void.reports.conf";
  if (arguments.size() > 0) {
    config = arguments[0];
  }
  if (config.isEmpty()) {
    lvd::qStdErr() << QString("missing config file") << Qt::endl;
    qcommandlineparser.showHelp(1);
  }

  LVD_LOG_D() << "config:"
              << config;

  // ----------

  try {
    lvd::reports::Reports reports(config);

    QObject::connect(&reports, &lvd::reports::Reports::success,
    [] (const QString& message) {
      if (!message.isEmpty()) {
        lvd::qStdOut() << message << Qt::endl;
      }

      lvd::Application::exit(0);
    });

    QObject::connect(&reports, &lvd::reports::Reports::failure,
    [] (const QString& message) {
      if (!message.isEmpty()) {
        lvd::qStdErr() << message << Qt::endl;
      }

      lvd::Application::exit(1);
    });

    QMetaObject::invokeMethod(&reports, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    return app.exec();
  }
  catch (const std::exception& ex) {
    LVD_LOG_C() << "exception:" << ex.what();
  }
  catch (...) {
    LVD_LOG_C() << "exception!";
  }

  return EXIT_FAILURE;
}
