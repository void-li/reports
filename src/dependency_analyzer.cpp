/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "dependency_analyzer.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QProcess>
#include <QRegularExpressionMatch>
#include <QThread>
#include <QVector>

#include "lvd/string.hpp"

#include "depend.hpp"

// ----------

namespace lvd::reports {

DependencyAnalyzer::DependencyAnalyzer(QObject* parent)
    : Analyzer(parent) {
  LVD_LOG_T();

  dependency_analyzer_impl_ = DependencyAnalyzerImpl::create(this);

  exe_expression_.setPattern(
    "Linking C(XX|) executable "  "(?<path>.+)"
  );

  lib_expression_.setPattern(
    "Linking C(XX|) shared library (?<path>(.+/|)lib.+\\.so)"
  );

  ldd_expression_.setPattern(
    "=> (?<path>\\/.+) \\(0x[0-9a-z]+\\)"
  );
}

DependencyAnalyzer::~DependencyAnalyzer() {
  LVD_LOG_T();
}

// ----------

void DependencyAnalyzer::analyze_impl(const QString& line) {
  LVD_LOG_T() << line;

  if (dependency_analyzer_impl_ == nullptr) {
    LVD_LOG_I() << "dependency analyzer unavailable";
    return;
  }

  QRegularExpressionMatch match;

  if (!match.isValid() || !match.hasMatch()) {
    match = exe_expression_.match(line);
  }
  if (!match.isValid() || !match.hasMatch()) {
    match = lib_expression_.match(line);
  }

  if ( match.isValid() &&  match.hasMatch()) {
    QString path = match.captured("path");

    if (QFile::exists(path)) {
      analyze_path(path);
      return;
    }

    QDirIterator qdiriterator(".", { path },
                              QDir::NoFilter,
                              QDirIterator::Subdirectories);

    while (qdiriterator.hasNext()) {
      QString path = qdiriterator.next();

      if (QFile::exists(path)) {
        analyze_path(path);
        return;
      }
    }
  }
}

void DependencyAnalyzer::analyze_path(const QString& path) {
  LVD_LOG_T() << path;

  QProcess qprocess;
  qprocess.start("ldd", { path });
  qprocess.waitForStarted();

  while (qprocess.state() == QProcess::Running || !qprocess.atEnd()) {
    qprocess.waitForReadyRead();
    bool readLine = false;

    while (qprocess.canReadLine()) {
      QByteArray line = strip(qprocess.readLine());
      readLine = true;

      LVD_LOG_D() << "parsing line:"
                  << line;

      auto match = ldd_expression_.match(line);
      if (match.hasMatch()) {
        QString path = match.captured("path");

        Depend::s depends = dependency_analyzer_impl_->analyze(path);

        for (const Depend& depend : depends) {
          emit this->depend(depend);
        }
      }
      else {
        LVD_LOG_D() << "invalid line:"
                    << line;
      }
    }

    if (!readLine) {
      QThread::msleep(1);
    }
  }
}

}  // namespace lvd::reports
