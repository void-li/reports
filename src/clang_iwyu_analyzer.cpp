/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "clang_iwyu_analyzer.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QFileInfo>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "report.hpp"

// ----------

namespace lvd::reports {

ClangIwyuAnalyzer::ClangIwyuAnalyzer(QObject* parent)
    : Analyzer(parent) {
  LVD_LOG_T();

  search_create_expression_.setPattern(
    "(?<path>.+) (?<mesg>should add" " these lines:)$"
  ); Q_ASSERT(search_create_expression_.isValid());

  search_remove_expression_.setPattern(
    "(?<path>.+) (?<mesg>should remove these lines:)$"
  ); Q_ASSERT(search_remove_expression_.isValid());

  inside_matchi_expression_.setPattern(
    "^(- |)#include (<[^>]+>|\"[^\"]+\")[ ]*(\\/\\/ lines (?<line>[0-9]+)-|)"
  ); Q_ASSERT(inside_matchi_expression_.isValid());

  inside_matchc_expression_.setPattern(
    "^(- |)class "   "[^ ]+;"          "[ ]*(\\/\\/ lines (?<line>[0-9]+)-|)"
  ); Q_ASSERT(inside_matchc_expression_.isValid());
}

ClangIwyuAnalyzer::~ClangIwyuAnalyzer() {
  LVD_LOG_T();
}

// ----------

void ClangIwyuAnalyzer::analyze_impl(const QString& line) {
  LVD_LOG_T() << line;

  LVD_LOG_D() << "state"
              << state_.to_string();

  switch (state_) {
    case State::Search: {
      QRegularExpressionMatch match;

      if (!match.hasMatch()) {
        if (match = search_create_expression_.match(line);
            match.hasMatch()) {
          LVD_LOG_D() << "switch to create state";
          state_ = State::Create;
        }
      }

      if (!match.hasMatch()) {
        if (match = search_remove_expression_.match(line);
            match.hasMatch()) {
          LVD_LOG_D() << "switch to remove state";
          state_ = State::Remove;
        }
      }

      if ( match.hasMatch()) {
        description_   = match.captured( 0x00 );
        fingerprint_   = "";

        location_path_ = match.captured("path");
        location_line_ = "";
      }
    } break;

    case State::Create:
    case State::Remove: {
      QRegularExpressionMatch match;

      if (!match.hasMatch()) {
        match = inside_matchi_expression_.match(line);
      }

      if (!match.hasMatch()) {
        match = inside_matchc_expression_.match(line);
      }

      if ( match.hasMatch()) {
        QString description   = QString("%1\n%2").arg(description_, line);

        QString location_path = location_path_;
        QString location_line = match.captured("line");

        if (location_line.isEmpty()) {
          location_line = "0";
        }

        const QFileInfo qfileinfo(location_path);
        location_path = qfileinfo.absoluteFilePath();

        LVD_LOG_D() << "new message"
                    << LVD_LOGLINE(description);

        Report message(description,
                       QString(),
                       location_path,
                       location_line);

        emit this->report(message);
      }
      else {
        LVD_LOG_D() << "switch to search state";
        state_ = State::Search;
      }
    } break;

    LVD_DEFAULT:
      LVD_THROW_IMPOSSIBLE;
      LVD_DEFAULT_BREAK;
  }
}

}  // namespace lvd::reports
