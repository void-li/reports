/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QRegularExpression>
#include <QString>

#include "lvd/logger.hpp"

#include "analyzer.hpp"
#include "dependency_analyzer_impl.hpp"

// ----------

namespace lvd::reports {

class DependencyAnalyzer : public Analyzer {
  Q_OBJECT LVD_LOGGER

 public:
  DependencyAnalyzer        (QObject* parent = nullptr);
  ~DependencyAnalyzer() override;

  static
  DependencyAnalyzer* create(QObject* parent = nullptr) {
    return new DependencyAnalyzer(parent);
  }

 private:
  void analyze_impl(const QString& line) override;
  void analyze_path(const QString& path);

 private:
  DependencyAnalyzerImpl* dependency_analyzer_impl_ = nullptr;

  QRegularExpression exe_expression_;
  QRegularExpression lib_expression_;

  QRegularExpression ldd_expression_;
};

DECLARE_ANALYZER(DependencyAnalyzer, dependency_analyzer);

}  // namespace lvd::reports
