/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "reports_executor.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>

#include "lvd/shield.hpp"

// ----------

namespace lvd::reports {

Reports::Executor::Executor(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();
}

Reports::Executor::~Executor() {
  LVD_LOG_T();

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    remove_process();
  };
}

// ----------

void Reports::Executor::execute(const QString& command) {
  LVD_LOG_T();
  LVD_SHIELD;

  create_process();

  qprocess_->start("sh", { "-c", command });

  LVD_LOG_D() << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  LVD_SHIELD_FUN(&Executor::failure);
}

// ----------

void Reports::Executor::create_process() {
  LVD_LOG_T();

  Q_ASSERT(qprocess_ == nullptr);

  remove_process();
  qprocess_ = new QProcess(this);

  connect(qprocess_, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
          this, &Reports::Executor::on_finished);

  connect(qprocess_, &QProcess::errorOccurred,
          this, &Reports::Executor::on_failure);

  connect(qprocess_, &QProcess::readyReadStandardOutput,
          this, &Reports::Executor::on_read_stdout);

  connect(qprocess_, &QProcess::readyReadStandardError,
          this, &Reports::Executor::on_read_stderr);
}

void Reports::Executor::remove_process() {
  LVD_LOG_T();

  if (qprocess_) {
    qprocess_->disconnect();

    if (qprocess_->state() == QProcess::Starting) {
      qprocess_->waitForStarted();
    }

    if (qprocess_->state() == QProcess::Running) {
      qprocess_->terminate();
      qprocess_->waitForFinished();
    }

    if (qprocess_->state() == QProcess::Running) {
      qprocess_->kill();
      qprocess_->waitForFinished();
    }
  }

  if (qprocess_) {
    qprocess_->deleteLater();
    qprocess_ = nullptr;
  }
}

void Reports::Executor::create_hard_timer() {
  LVD_LOG_T();

  Q_ASSERT(hard_timer_ == nullptr);

  remove_hard_timer();
  hard_timer_ = new QTimer(this);

  connect(hard_timer_, &QTimer::timeout,
          this, &Reports::Executor::on_hard_timeout);

  Q_ASSERT(hard_timeout_ >= 0);

  if (hard_timeout_ > 0) {
    hard_timer_->setInterval(hard_timeout_);
    hard_timer_->start();
  }
}

void Reports::Executor::remove_hard_timer() {
  LVD_LOG_T();

  if (hard_timer_) {
    hard_timer_->deleteLater();
    hard_timer_ = nullptr;
  }
}

void Reports::Executor::create_soft_timer() {
  LVD_LOG_T();

  Q_ASSERT(soft_timer_ == nullptr);

  remove_soft_timer();
  soft_timer_ = new QTimer(this);

  connect(soft_timer_, &QTimer::timeout,
          this, &Reports::Executor::on_soft_timeout);

  Q_ASSERT(soft_timeout_ >= 0);

  if (soft_timeout_ > 0) {
    soft_timer_->setInterval(soft_timeout_);
    soft_timer_->start();
  }
}

void Reports::Executor::remove_soft_timer() {
  LVD_LOG_T();

  if (soft_timer_) {
    soft_timer_->deleteLater();
    soft_timer_ = nullptr;
  }
}

void Reports::Executor::notify_soft_timer() {
  LVD_LOG_T();

  if (soft_timer_ && soft_timer_->interval() > 0) {
    soft_timer_->start();
  }
}

void Reports::Executor::ignore_soft_timer() {
  LVD_LOG_T();

  if (soft_timer_) {
    soft_timer_->stop();
  }
}

// ----------

void Reports::Executor::on_finished() {
  LVD_LOG_T();
  LVD_SHIELD;

  Reports::Executor::on_read_stdout();
  Reports::Executor::on_read_stderr();

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    remove_process();
  };

  if (qprocess_) {
    LVD_LOG_D() << "process result"
                << qprocess_->program()
                << qprocess_->arguments()
                << qprocess_->exitStatus()
                << qprocess_->exitCode();

    if (   qprocess_->exitStatus() == QProcess::NormalExit
        && qprocess_->exitCode()   == 0) {
      emit success();
      return;
    }

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "process failed"
                        << qprocess_->program()
                        << qprocess_->arguments()
                        << qprocess_->exitStatus()
                        << qprocess_->exitCode();

    emit failure(message);
    return;
  }

  LVD_THROW_IMPOSSIBLE;

  LVD_SHIELD_FUN(&Executor::failure);
}

void Reports::Executor::on_failure(QProcess::ProcessError process_error) {
  LVD_LOG_T();
  LVD_SHIELD;

  if (process_error == QProcess::FailedToStart) {
    remove_soft_timer();
    remove_hard_timer();

    LVD_FINALLY {
      remove_process();
    };

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "process failed to start"
                        << qprocess_->program()
                        << qprocess_->arguments();

    emit failure(message);
    return;
  }

  LVD_SHIELD_FUN(&Executor::failure);
}

void Reports::Executor::on_read_stdout() {
  LVD_LOG_T();
  LVD_SHIELD;

  notify_soft_timer();
  qprocess_->setReadChannel(QProcess::StandardOutput);

  while (qprocess_->canReadLine()) {
    QString line = qprocess_->readLine();
    lvd::qStdOut() << line << Qt::flush;

    emit analyze(line);
  }

  LVD_SHIELD_FUN(&Executor::failure);
}

void Reports::Executor::on_read_stderr() {
  LVD_LOG_T();
  LVD_SHIELD;

  notify_soft_timer();
  qprocess_->setReadChannel(QProcess::StandardError );

  while (qprocess_->canReadLine()) {
    QString line = qprocess_->readLine();
    lvd::qStdErr() << line << Qt::flush;

    emit analyze(line);
  }

  LVD_SHIELD_FUN(&Executor::failure);
}

void Reports::Executor::on_hard_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    remove_process();
  };

  LVD_LOGBUF message;
  LVD_LOG_W(&message) << "hard timeout after"
                      << hard_timeout_
                      << "milliseconds during"
                      << qprocess_->program()
                      << qprocess_->arguments();

  emit failure(message);
  return;

  LVD_SHIELD_FUN(&Executor::failure);
}

void Reports::Executor::on_soft_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    remove_process();
  };

  LVD_LOGBUF message;
  LVD_LOG_W(&message) << "soft timeout after"
                      << soft_timeout_
                      << "milliseconds during"
                      << qprocess_->program()
                      << qprocess_->arguments();

  emit failure(message);
  return;

  LVD_SHIELD_FUN(&Executor::failure);
}

}  // namespace lvd::reports
