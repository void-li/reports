/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QHash>
#include <QObject>
#include <QRegularExpression>
#include <QString>

#include "lvd/logger.hpp"

#include "depend.hpp"
#include "dependency_analyzer_impl.hpp"
#include "vulnerability.hpp"

// ----------

namespace lvd::reports {

class DependencyAnalyzerImplArch : public DependencyAnalyzerImpl {
  Q_OBJECT LVD_LOGGER

 public:
  DependencyAnalyzerImplArch        (QObject* parent = nullptr);
  ~DependencyAnalyzerImplArch() override;

  static
  DependencyAnalyzerImplArch* create(QObject* parent = nullptr) {
    return new DependencyAnalyzerImplArch(parent);
  }

 private:
  Depend::s analyze_impl(const QString& path) override;

 private:
  QRegularExpression owned_expression_;
  QRegularExpression audit_expression_;

  QHash<QString, Vulnerabilities> vulnerabilities_;
};

DECLARE_DEPENDENCY_ANALYZER_IMPL(DependencyAnalyzerImplArch, arch)

}  // namespace lvd::reports
