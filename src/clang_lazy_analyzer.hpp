/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QRegularExpression>
#include <QString>

#include "lvd/logger.hpp"

#include "analyzer.hpp"

// ----------

namespace lvd::reports {

class ClangLazyAnalyzer : public Analyzer {
  Q_OBJECT LVD_LOGGER

 public:
  ClangLazyAnalyzer        (QObject* parent = nullptr);
  ~ClangLazyAnalyzer() override;

  static
  ClangLazyAnalyzer* create(QObject* parent = nullptr) {
    return new ClangLazyAnalyzer(parent);
  }

 private:
  void analyze_impl(const QString& line) override;

 private:
  QRegularExpression search_expression_;
};

DECLARE_ANALYZER(ClangLazyAnalyzer, clang_lazy_analyzer);

}  // namespace lvd::reports
