/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

#include <QObject>
#include <QString>

// ----------

int main(int argc, char* argv[]) {
  const quint32 a =   42 ;
  QObject::connect(nullptr, &QObject::destroyed, [&a] { });

  const QString b = "b42";
  b.mid(1).toInt();

  const QString c = QString("%1 %2").arg(b).arg(b);
  Q_UNUSED(c);

  return 0;
}
