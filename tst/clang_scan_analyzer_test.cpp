/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QDir>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QTemporaryDir>
#include <QVariant>

#include "lvd/metatype.hpp"

#include "clang_scan_analyzer.hpp"
#include "report.hpp"
#include "test_config.hpp"
#include "vulnerability.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::reports;

// ----------

namespace {
// clazy:excludeall=non-pod-global-static

const QString singlex_data =
"/clang_scan_analyzer_test/main.cpp:15:15: warning: Division by zero [core.DivideZero]\n"
"    int j = 1 / i;\n"
"            ~~^~~\n"
;

const QString singlex_data_mesg0 = "Division by zero [core.DivideZero]";
const QString singlex_data_path0 = "/clang_scan_analyzer_test/main.cpp";
const QString singlex_data_line0 = "15";

const QString several_data =
"/clang_scan_analyzer_test/main.cpp:15:15: warning: Division by zero [core.DivideZero]\n"
"    int j = 1 / i;\n"
"            ~~^~~\n"
"/clang_scan_analyzer_test/main.cpp:24:8: warning: Value stored to 'b' during its initialization is never read [deadcode.DeadStores]\n"
"  int  b = *a;\n"
"       ^   ~~\n"
"/clang_scan_analyzer_test/main.cpp:24:12: warning: Dereference of null pointer (loaded from variable 'a') [core.NullDereference]\n"
"  int  b = *a;\n"
"           ^~\n"
;

const QString several_data_mesg0 = "Division by zero [core.DivideZero]";
const QString several_data_path0 = "/clang_scan_analyzer_test/main.cpp";
const QString several_data_line0 = "15";

const QString several_data_mesg1 = "Value stored to 'b' during its initialization is never read [deadcode.DeadStores]";
const QString several_data_path1 = "/clang_scan_analyzer_test/main.cpp";
const QString several_data_line1 = "24";

const QString several_data_mesg2 = "Dereference of null pointer (loaded from variable 'a') [core.NullDereference]";
const QString several_data_path2 = "/clang_scan_analyzer_test/main.cpp";
const QString several_data_line2 = "24";

const QString complex_data =
"scan-build: Using '/usr/bin/clang-7' for static analysis\n"
"[ 50%] Building CXX object CMakeFiles/main.dir/main.cpp.o\n"
"/clang_scan_analyzer_test/main.cpp:16:15: warning: Division by zero [core.DivideZero]\n"
"    int j = 1 / i;\n"
"            ~~^~~\n"
"/clang_scan_analyzer_test/main.cpp:25:8: warning: Value stored to 'b' during its initialization is never read [deadcode.DeadStores]\n"
"  int  b = *a;\n"
"       ^   ~~\n"
"/clang_scan_analyzer_test/main.cpp:25:12: warning: Dereference of null pointer (loaded from variable 'a') [core.NullDereference]\n"
"  int  b = *a;\n"
"           ^~\n"
"3 warnings generated.\n"
"[100%] Linking CXX executable main\n"
"[100%] Built target main\n"
"scan-build: 3 bugs found.\n"
"scan-build: Run 'scan-view /tmp/scan-build-2019-03-04-201550-8321-1' to examine bug reports.\n"
;

const QString complex_data_mesg0 = "Division by zero [core.DivideZero]";
const QString complex_data_path0 = "/clang_scan_analyzer_test/main.cpp";
const QString complex_data_line0 = "16";

const QString complex_data_mesg1 = "Value stored to 'b' during its initialization is never read [deadcode.DeadStores]";
const QString complex_data_path1 = "/clang_scan_analyzer_test/main.cpp";
const QString complex_data_line1 = "25";

const QString complex_data_mesg2 = "Dereference of null pointer (loaded from variable 'a') [core.NullDereference]";
const QString complex_data_path2 = "/clang_scan_analyzer_test/main.cpp";
const QString complex_data_line2 = "25";

}  // namespace

// ----------

class ClangScanAnalyzerTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ClangScanAnalyzer        ();
    ClangScanAnalyzer::create()->deleteLater();
  }

  // ----------

  void singlex() {
    ClangScanAnalyzer clang_scan_analyzer;

    QSignalSpy spy(&clang_scan_analyzer, &ClangScanAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = singlex_data.split("\n");
      for (const QString& line : lines) {
        clang_scan_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].value<Report>().description()  , singlex_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), singlex_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), singlex_data_line0);
  }

  void several() {
    ClangScanAnalyzer clang_scan_analyzer;

    QSignalSpy spy(&clang_scan_analyzer, &ClangScanAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = several_data.split("\n");
      for (const QString& line : lines) {
        clang_scan_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 3);

    QCOMPARE(spy[0][0].value<Report>().description()  , several_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), several_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), several_data_line0);

    QCOMPARE(spy[1][0].value<Report>().description()  , several_data_mesg1);
    QCOMPARE(spy[1][0].value<Report>().location_path(), several_data_path1);
    QCOMPARE(spy[1][0].value<Report>().location_line(), several_data_line1);

    QCOMPARE(spy[2][0].value<Report>().description()  , several_data_mesg2);
    QCOMPARE(spy[2][0].value<Report>().location_path(), several_data_path2);
    QCOMPARE(spy[2][0].value<Report>().location_line(), several_data_line2);
  }

  void complex() {
    ClangScanAnalyzer clang_scan_analyzer;

    QSignalSpy spy(&clang_scan_analyzer, &ClangScanAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = complex_data.split("\n");
      for (const QString& line : lines) {
        clang_scan_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 3);

    QCOMPARE(spy[0][0].value<Report>().description()  , complex_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), complex_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), complex_data_line0);

    QCOMPARE(spy[1][0].value<Report>().description()  , complex_data_mesg1);
    QCOMPARE(spy[1][0].value<Report>().location_path(), complex_data_path1);
    QCOMPARE(spy[1][0].value<Report>().location_line(), complex_data_line1);

    QCOMPARE(spy[2][0].value<Report>().description()  , complex_data_mesg2);
    QCOMPARE(spy[2][0].value<Report>().location_path(), complex_data_path2);
    QCOMPARE(spy[2][0].value<Report>().location_line(), complex_data_line2);
  }

  // ----------

  void calling() {
    QTemporaryDir qtemporarydir;
    QVERIFY(qtemporarydir.isValid());

    QString current_path = QDir::currentPath();
    QDir::setCurrent(qtemporarydir.path());

    LVD_FINALLY {
      QDir::setCurrent(current_path);
    };

    int ret;

    ret = execute("scan-build", "-o", "scan-build", "--use-c++=clang++", "cmake", "-GNinja", Current_Source_Dir() + "/clang_scan_analyzer_test");
    QCOMPARE(ret, 0);

    ret = execute("scan-build", "-o", "scan-build", "--use-c++=clang++", "ninja");
    QCOMPARE(ret, 0);

    execute_stdout_.replace(Current_Source_Dir().toLocal8Bit(), "");
    execute_stderr_.replace(Current_Source_Dir().toLocal8Bit(), "");

    ClangScanAnalyzer clang_scan_analyzer;

    QSignalSpy spy(&clang_scan_analyzer, &ClangScanAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = QString::fromLocal8Bit(execute_stdout_).split('\n');
      for (const QString& line : lines) {
        clang_scan_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 3);

    QCOMPARE(spy[0][0].value<Report>().description()  , complex_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), complex_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), complex_data_line0);

    QCOMPARE(spy[1][0].value<Report>().description()  , complex_data_mesg1);
    QCOMPARE(spy[1][0].value<Report>().location_path(), complex_data_path1);
    QCOMPARE(spy[1][0].value<Report>().location_line(), complex_data_line1);

    QCOMPARE(spy[2][0].value<Report>().description()  , complex_data_mesg2);
    QCOMPARE(spy[2][0].value<Report>().location_path(), complex_data_path2);
    QCOMPARE(spy[2][0].value<Report>().location_line(), complex_data_line2);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();

    Metatype::metatype();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ClangScanAnalyzerTest)
#include "clang_scan_analyzer_test.moc"  // IWYU pragma: keep
