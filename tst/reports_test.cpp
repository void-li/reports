/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <functional>

#include <QByteArray>
#include <QCoreApplication>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonValueRef>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QTemporaryDir>

#include "depend.hpp"
#include "report.hpp"
#include "reports.hpp"
#include "reports_test_analyzer.hpp"
#include "vulnerability.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::reports;

// ----------

class ReportsTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Reports reports(config_);
  }

  // ----------

  void execute() {
    QFETCH(QStringList, sections);
    QFETCH(QStringList, commands);

    QFETCH(QString    , stage   );
    QFETCH(QStringList, existing);

    configure(sections, [&] (QSettings& qsettings) {
      qsettings.setValue("reports_test_execute_analyzer", "Mera Luna");

      if        (commands.size() == 1) {
        qsettings.setValue(stage, commands[0]);
      } else if (commands.size() >= 1) {
        qsettings.beginWriteArray(stage);
        LVD_FINALLY { qsettings.endArray(); };

        for (int i = 0; i < commands.size(); i ++) {
          qsettings.setArrayIndex(i);
          qsettings.setValue(stage, commands[i]);
        }
      }
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    for (QString& filename : existing) {
      QVERIFY2(QFile::exists(filename), qPrintable(filename));
    }
  }

  void execute_data() {
    QTest::addColumn<QStringList>("sections");
    QTest::addColumn<QStringList>("commands");

    QTest::addColumn<QString    >("stage"   );
    QTest::addColumn<QStringList>("existing");

    QStringList stages({
      "before", "during", "beyond"
    });

    for (QString& stage : stages) {
      QTest::addRow("single single %s", qPrintable(stage))
          << QStringList({ "Mera Luna" })
          << QString("^sh -c \"touch MeraLuna$(ls MeraLuna* 2>/dev/null | wc -l).txt\"")
             .repeated(1).split("^", Qt::SkipEmptyParts)
          << stage
          << QStringList({ "MeraLuna0.txt" });
    }

    for (QString& stage : stages) {
      QTest::addRow("single double %s", qPrintable(stage))
          << QStringList({ "Mera Luna" })
          << QString("^sh -c \"touch MeraLuna$(ls MeraLuna* 2>/dev/null | wc -l).txt\"")
             .repeated(2).split("^", Qt::SkipEmptyParts)
          << stage
          << QStringList({ "MeraLuna0.txt", "MeraLuna1.txt" });
    }

    for (QString& stage : stages) {
      QTest::addRow("double single %s", qPrintable(stage))
          << QStringList({ "Mera Luna", "Star Dust" })
          << QString("^sh -c \"touch MeraLuna$(ls MeraLuna* 2>/dev/null | wc -l).txt\"")
             .repeated(1).split("^", Qt::SkipEmptyParts)
          << stage
          << QStringList({ "MeraLuna0.txt", "MeraLuna1.txt" });
    }

    for (QString& stage : stages) {
      QTest::addRow("double double %s", qPrintable(stage))
          << QStringList({ "Mera Luna", "Star Dust" })
          << QString("^sh -c \"touch MeraLuna$(ls MeraLuna* 2>/dev/null | wc -l).txt\"")
             .repeated(2).split("^", Qt::SkipEmptyParts)
          << stage
          << QStringList({ "MeraLuna0.txt", "MeraLuna1.txt", "MeraLuna2.txt", "MeraLuna3.txt" });
    }
  }

  void execute_invalid() {
    QFETCH(QString, stage);

    configure([&] (QSettings& qsettings) {
      qsettings.setValue(stage, "pneumonoultramicroscopicsilicovolcanoconiosis");
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void execute_invalid_data() {
    QTest::addColumn<QString>("stage");

    QTest::addRow("before") << "before";
    QTest::addRow("during") << "during";
    QTest::addRow("beyond") << "beyond";
  }

  void execute_outputs() {
    QFETCH(QStringList, sections);
    QFETCH(QStringList, commands);

    QFETCH(QString    , stage   );
    QFETCH(QStringList, messages);

    configure(sections, [&] (QSettings& qsettings) {
      qsettings.setValue("reports_test_execute_analyzer", "Mera Luna");

      if        (commands.size() == 1) {
        qsettings.setValue(stage, commands[0]);
      } else if (commands.size() >= 1) {
        qsettings.beginWriteArray(stage);
        LVD_FINALLY { qsettings.endArray(); };

        for (int i = 0; i < commands.size(); i ++) {
          qsettings.setArrayIndex(i);
          qsettings.setValue(stage, commands[i]);
        }
      }
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(ReportsTestExecuteAnalyzer::lines.size(),
             messages.size());

    for (int i = 0; i < ReportsTestExecuteAnalyzer::lines.size(); i ++) {
      QCOMPARE(ReportsTestExecuteAnalyzer::lines[i],
               messages[i]);
    }
  }

  void execute_outputs_data() {
    QTest::addColumn<QStringList>("sections");
    QTest::addColumn<QStringList>("commands");

    QTest::addColumn<QString    >("stage"   );
    QTest::addColumn<QStringList>("messages");

    QStringList stages({
      "before", "during", "beyond"
    });

    for (QString& stage : stages) {
      QTest::addRow("single single %s", qPrintable(stage))
          << QStringList({      "Mera Luna"                   })
          << QStringList({ "echo Mera Luna"                   })
          << stage << (stage == "during"
          ?  QStringList({      "Mera Luna"                   })
          :  QStringList());
    }

    for (QString& stage : stages) {
      QTest::addRow("single double %s", qPrintable(stage))
          << QStringList({      "Mera Luna"                   })
          << QStringList({ "echo Mera Luna", "echo Star Dust" })
          << stage << (stage == "during"
          ?  QStringList({      "Mera Luna",      "Star Dust" })
          :  QStringList());
    }

    for (QString& stage : stages) {
      QTest::addRow("double single %s", qPrintable(stage))
          << QStringList({      "Mera Luna",      "Star Dust" })
          << QStringList({ "echo Mera Luna"                   })
          << stage << (stage == "during"
          ?  QStringList({      "Mera Luna",      "Mera Luna" })
          :  QStringList());
    }

    for (QString& stage : stages) {
      QTest::addRow("double double %s", qPrintable(stage))
          << QStringList({      "Mera Luna",      "Star Dust" })
          << QStringList({ "echo Mera Luna", "echo Star Dust" })
          << stage << (stage == "during"
          ?  QStringList({      "Mera Luna",      "Star Dust",      "Mera Luna",      "Star Dust" })
          :  QStringList());
    }
  }

  // ----------

  void invalid_analyzer() {
    configure([&] (QSettings& qsettings) {
      qsettings.setValue("reports_test_invalid_analyzer", "Mera Luna");
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

  void absolute_path() {
    QFETCH(QString, stage);

    QTemporaryDir qtemporarydir(QCoreApplication::applicationName() + "-XXXXXX");
    QVERIFY2(qtemporarydir.isValid(), qPrintable(qtemporarydir.errorString()));

    configure([&] (QSettings& qsettings) {
      QString path = QFileInfo(qtemporarydir.path()).filePath();
      QVERIFY(path.startsWith("/"));

      qsettings.setValue("path", path);
      qsettings.setValue(stage, "touch alive");
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QString path = qtemporarydir.filePath("alive");
    QVERIFY(QFile::exists(path));
  }

  void absolute_path_data() {
    QTest::addColumn<QString>("stage");

    QTest::addRow("before") << "before";
    QTest::addRow("during") << "during";
    QTest::addRow("beyond") << "beyond";
  }

  void relative_path() {
    QFETCH(QString, stage);

    QTemporaryDir qtemporarydir(QCoreApplication::applicationName() + "-XXXXXX");
    QVERIFY2(qtemporarydir.isValid(), qPrintable(qtemporarydir.errorString()));

    configure([&] (QSettings& qsettings) {
      QString path = QFileInfo(qtemporarydir.path()).fileName();
      QVERIFY(path.startsWith("/") == false);

      qsettings.setValue("path", path);
      qsettings.setValue(stage, "touch alive");
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QString path = qtemporarydir.filePath("alive");
    QVERIFY(QFile::exists(path));
  }

  void relative_path_data() {
    QTest::addColumn<QString>("stage");

    QTest::addRow("before") << "before";
    QTest::addRow("during") << "during";
    QTest::addRow("beyond") << "beyond";
  }

  void recreate_path() {
    QFETCH(QString, stage);

    QTemporaryDir qtemporarydir(QCoreApplication::applicationName() + "-XXXXXX");
    QVERIFY2(qtemporarydir.isValid(), qPrintable(qtemporarydir.errorString()));

    QVERIFY(qtemporarydir.remove());

    configure([&] (QSettings& qsettings) {
      QString path = QFileInfo(qtemporarydir.path()).filePath();
      QVERIFY(path.startsWith("/"));

      qsettings.setValue("path", path);
      qsettings.setValue(stage, "touch alive");
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QString path = qtemporarydir.filePath("alive");
    QVERIFY(QFile::exists(path));
  }

  void recreate_path_data() {
    QTest::addColumn<QString>("stage");

    QTest::addRow("before") << "before";
    QTest::addRow("during") << "during";
    QTest::addRow("beyond") << "beyond";
  }

  // ----------

  void keep_path() {
    QFETCH(QString, stage);

    QTemporaryDir qtemporarydir(QCoreApplication::applicationName() + "-XXXXXX");
    QVERIFY2(qtemporarydir.isValid(), qPrintable(qtemporarydir.errorString()));

    QVERIFY(qtemporarydir.remove());

    configure([&] (QSettings& qsettings) {
      QString path = QFileInfo(qtemporarydir.path()).filePath();
      QVERIFY(path.startsWith("/"));

      qsettings.setValue("path", path);
      qsettings.setValue("keep", "true");

      if (!stage.isEmpty()) {
        qsettings.setValue(stage, "echo Mera Luna");
      }
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QString path = qtemporarydir.path();

    if (!stage.isEmpty()) {
      QVERIFY(QFile::exists(path));
    } else {
      QVERIFY(QFile::exists(path) == false);
    }
  }

  void keep_path_data() {
    QTest::addColumn<QString>("stage");

    QTest::addRow("")       << "";
    QTest::addRow("before") << "before";
    QTest::addRow("during") << "during";
    QTest::addRow("beyond") << "beyond";
  }

  void kill_path() {
    QFETCH(QString, stage);

    QTemporaryDir qtemporarydir(QCoreApplication::applicationName() + "-XXXXXX");
    QVERIFY2(qtemporarydir.isValid(), qPrintable(qtemporarydir.errorString()));

    QVERIFY(qtemporarydir.remove());

    configure([&] (QSettings& qsettings) {
      QString path = QFileInfo(qtemporarydir.path()).filePath();
      QVERIFY(path.startsWith("/"));

      qsettings.setValue("path", path);
      qsettings.setValue("keep", "false");

      if (!stage.isEmpty()) {
        qsettings.setValue(stage, "echo Mera Luna");
      }
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QString path = qtemporarydir.path();

    if (!stage.isEmpty()) {
      QVERIFY(QFile::exists(path) == false);
    } else {
      QVERIFY(QFile::exists(path) == false);
    }
  }

  void kill_path_data() {
    QTest::addColumn<QString>("stage");

    QTest::addRow("")       << "";
    QTest::addRow("before") << "before";
    QTest::addRow("during") << "during";
    QTest::addRow("beyond") << "beyond";
  }

  // ----------

  void filters() {
    QFETCH(QString    , filter_name);
    QFETCH(QStringList, filter_data);
    QFETCH(bool       , second_item);

    configure([&] (QSettings& qsettings) {
      qsettings.setValue("reports_test_publish_analyzer", "Mera Luna");

      {
        qsettings.beginWriteArray("during");
        LVD_FINALLY { qsettings.endArray(); };

        qsettings.setArrayIndex(0);
        qsettings.setValue("during", "echo 1");

        qsettings.setArrayIndex(1);
        qsettings.setValue("during", "echo 1");

        qsettings.setArrayIndex(2);
        qsettings.setValue("during", "echo 1");
      }

      if        (filter_data.size() == 1) {
        qsettings.setValue(filter_name, filter_data[0]);
      } else if (filter_data.size() >= 1) {
        qsettings.beginWriteArray(filter_name);
        LVD_FINALLY { qsettings.endArray(); };

        for (int i = 0; i < filter_data.size(); i ++) {
          qsettings.setArrayIndex(i);
          qsettings.setValue(filter_name, filter_data[i]);
        }
      }

      qsettings.endGroup();

      qsettings.setValue("source", "/");
      qsettings.setValue("binary", "/");

      qsettings.beginGroup("dummy");
    });

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QFile qfile(reports_path_);
    qfile.open(QFile::ReadOnly);

    QVERIFY(qfile.isOpen());

    QByteArray data = qfile.readAll();

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isArray());

    auto qja = qjd.array();
    QCOMPARE(qja.size(), second_item ? 2 : 1);

    auto qjv0 = qja[0];
    QCOMPARE(qjv0.type(), QJsonValue::Object);
    auto qjo0 = qjv0.toObject();

    QCOMPARE(qjo0.value("description").toString(),
             "description #0");

    if (second_item) {
      auto qjv1 = qja[1];
      QCOMPARE(qjv1.type(), QJsonValue::Object);
      auto qjo1 = qjv1.toObject();

      QCOMPARE(qjo1.value("description").toString(),
               "description #2");
    }
  }

  void filters_data() {
    QTest::addColumn<QString    >("filter_name");
    QTest::addColumn<QStringList>("filter_data");
    QTest::addColumn<bool       >("second_item");

    QTest::addRow(  "description_filter singlex")
        <<   "description_filter" << QStringList({ "#1$"        }) << true ;

    QTest::addRow(  "description_filter several")
        <<   "description_filter" << QStringList({ "#1$", "#2$" }) << false;

    QTest::addRow(  "fingerprint_filter singlex")
        <<   "fingerprint_filter" << QStringList({ "#1$"        }) << true ;

    QTest::addRow(  "fingerprint_filter several")
        <<   "fingerprint_filter" << QStringList({ "#1$", "#2$" }) << false;

    QTest::addRow("location_path_filter singlex")
        << "location_path_filter" << QStringList({ "#1$"        }) << true ;

    QTest::addRow("location_path_filter several")
        << "location_path_filter" << QStringList({ "#1$", "#2$" }) << false;

    QTest::addRow("location_line_filter singlex")
        << "location_line_filter" << QStringList({ "^1$"        }) << true ;

    QTest::addRow("location_line_filter several")
        << "location_line_filter" << QStringList({ "^1$", "^2$" }) << false;
  }

  // ----------

 private:
  QByteArray sorting(const QString& path) {
    QByteArray qbytearray;

    [&] {
      configure([&] (QSettings& qsettings) {
        qsettings.setValue("reports_test_message_analyzer", "Mera Luna");

        {
          qsettings.beginWriteArray("during");
          LVD_FINALLY { qsettings.endArray(); };

          qsettings.setArrayIndex(0);
          qsettings.setValue("during", "echo Mera Luna");
        }

        qsettings.endGroup();

        qsettings.setValue("source", "/");
        qsettings.setValue("binary", "/");

        qsettings.beginGroup("dummy");
      });

      Reports reports(config_);

      QSignalSpy spy(&reports, &Reports::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        reports.execute();
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);

      QFile qfile(path);
      qfile.open(QFile::ReadOnly);

      QVERIFY(qfile.isOpen());

      qbytearray = qfile.readAll();
    }();

    return qbytearray;
  }

 private slots:
  void sorting_reports() {
    QFETCH(QString, fst_location_line);
    QFETCH(QString, snd_location_line);

    ReportsTestMessageAnalyzer::reports << Report(
      "Mera Luna #1", "Mera Luna #1",
      "/meraluna.cpp", fst_location_line
    );

    ReportsTestMessageAnalyzer::reports << Report(
      "Mera Luna #2", "Mera Luna #2",
      "/meraluna.cpp", snd_location_line
    );

    QByteArray data = sorting(reports_path_);

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isArray());

    auto qja = qjd.array();
    QCOMPARE(qja.size(), 2);

    auto qjv0 = qja[0];
    QCOMPARE(qjv0.type(), QJsonValue::Object);
    auto qjo0 = qjv0.toObject();

    auto qjv1 = qjo0.value("location");
    QCOMPARE(qjv1.type(), QJsonValue::Object);
    auto qjo1 = qjv1.toObject();

    auto qjv2 = qjo1.value("lines");
    QCOMPARE(qjv2.type(), QJsonValue::Object);
    auto qjo2 = qjv2.toObject();

    auto qjv3 = qja[1];
    QCOMPARE(qjv0.type(), QJsonValue::Object);
    auto qjo3 = qjv3.toObject();

    auto qjv4 = qjo3.value("location");
    QCOMPARE(qjv1.type(), QJsonValue::Object);
    auto qjo4 = qjv4.toObject();

    auto qjv5 = qjo4.value("lines");
    QCOMPARE(qjv2.type(), QJsonValue::Object);
    auto qjo5 = qjv5.toObject();

    QVERIFY(qjo2.value("begin").toDouble() < qjo5.value("begin").toDouble());
  }

  void sorting_reports_data() {
    QTest::addColumn<QString>("fst_location_line");
    QTest::addColumn<QString>("snd_location_line");

    QTest::addRow("smaller") <<   "23" << "42";
    QTest::addRow("greater") <<   "42" << "23";
    QTest::addRow("natural") << "2323" << "42";
  }

  void sorting_depends() {
    QFETCH(QString, fst_package);
    QFETCH(QString, fst_version);

    QFETCH(QString, snd_package);
    QFETCH(QString, snd_version);

    QFETCH(bool   , fst_primary);

    ReportsTestMessageAnalyzer::depends << Depend(
        fst_package, fst_version, {}
    );

    ReportsTestMessageAnalyzer::depends << Depend(
        snd_package, snd_version, {}
    );

    QByteArray data = sorting(depends_path_);

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isObject());

    auto qjo = qjd.object();

    auto qjv_defi = qjo.value("dependency_files");
    QCOMPARE(qjv_defi.type(), QJsonValue::Array);
    auto qja_defi = qjv_defi.toArray();

    auto qjv_depe = qja_defi.first();
    QCOMPARE(qjv_depe.type(), QJsonValue::Object);
    auto qjo_depe = qjv_depe.toObject();

         qjv_depe = qjo_depe.value("dependencies");
    QCOMPARE(qjv_depe.type(), QJsonValue::Array);
    auto qja_depe = qjv_depe.toArray();

    QCOMPARE(qja_depe.size(), 2);

    {
      int index = fst_primary ? 0 : 1;

      auto qjv_depe = qja_depe[index];
      QCOMPARE(qjv_depe.type(), QJsonValue::Object);
      auto qjo_depe = qjv_depe.toObject();

      auto qjv_pack = qjo_depe.value("package");
      QCOMPARE(qjv_pack.type(), QJsonValue::Object);
      auto qjo_pack = qjv_pack.toObject();

      auto qjv_name = qjo_pack.value("name");
      QCOMPARE(qjv_name.type(), QJsonValue::String);

      auto qjs_name = qjv_name.toString();
      QCOMPARE(qjs_name, fst_package);

      auto qjv_vers = qjo_depe.value("version");
      QCOMPARE(qjv_vers.type(), QJsonValue::String);

      auto qjs_vers = qjv_vers.toString();
      QCOMPARE(qjs_vers, fst_version);
    }

    {
      int index = fst_primary ? 1 : 0;

      auto qjv_depe = qja_depe[index];
      QCOMPARE(qjv_depe.type(), QJsonValue::Object);
      auto qjo_depe = qjv_depe.toObject();

      auto qjv_pack = qjo_depe.value("package");
      QCOMPARE(qjv_pack.type(), QJsonValue::Object);
      auto qjo_pack = qjv_pack.toObject();

      auto qjv_name = qjo_pack.value("name");
      QCOMPARE(qjv_name.type(), QJsonValue::String);

      auto qjs_name = qjv_name.toString();
      QCOMPARE(qjs_name, snd_package);

      auto qjv_vers = qjo_depe.value("version");
      QCOMPARE(qjv_vers.type(), QJsonValue::String);

      auto qjs_vers = qjv_vers.toString();
      QCOMPARE(qjs_vers, snd_version);
    }
  }

  void sorting_depends_data() {
    QTest::addColumn<QString>("fst_package");
    QTest::addColumn<QString>("fst_version");

    QTest::addColumn<QString>("snd_package");
    QTest::addColumn<QString>("snd_version");

    QTest::addColumn<bool   >("fst_primary");

    QTest::addRow("greater package") << "Mera" << "v0.0.0"
                                     << "Luna" << "v0.0.0"
                                     << false;

    QTest::addRow("smaller package") << "Luna" << "v0.0.0"
                                     << "Mera" << "v0.0.0"
                                     << true;

    QTest::addRow("greater version") << "Mera" << "v1.0.0"
                                     << "Mera" << "v0.0.0"
                                     << false;

    QTest::addRow("smaller version") << "Mera" << "v0.0.0"
                                     << "Mera" << "v1.0.0"
                                     << true;

    QTest::addRow("package version") << "Mera" << "v1.0.0"
                                     << "Luna" << "v0.0.0"
                                     << false;
  }

  // ----------

 private:
  QByteArray publish(const QString&     path,
                     const QStringList& sections,
                     const QStringList& commands,
                     const QString&     stage) {
    QByteArray qbytearray;

    [&] {
      configure(sections, [&] (QSettings& qsettings) {
        qsettings.setValue("reports_test_publish_analyzer", "Mera Luna");

        if        (commands.size() == 1) {
          qsettings.setValue(stage, commands[0]);
        } else if (commands.size() >= 1) {
          qsettings.beginWriteArray(stage);
          LVD_FINALLY { qsettings.endArray(); };

          for (int i = 0; i < commands.size(); i ++) {
            qsettings.setArrayIndex(i);
            qsettings.setValue(stage, commands[i]);
          }
        }

        qsettings.endGroup();

        qsettings.setValue("source", "/");
        qsettings.setValue("binary", "/");

        qsettings.beginGroup("dummy");
      });

      Reports reports(config_);

      QSignalSpy spy(&reports, &Reports::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        reports.execute();
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);

      QFile qfile(path);
      qfile.open(QFile::ReadOnly);

      QVERIFY(qfile.isOpen());

      qbytearray = qfile.readAll();
    }();

    return qbytearray;
  }

 private slots:
  void publish_reports() {
    QFETCH(QStringList, sections);
    QFETCH(QStringList, commands);

    QFETCH(QString    , stage   );
    QFETCH(int        , sizes   );

    QByteArray data = publish(reports_path_, sections, commands, stage);

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isArray());

    auto qja = qjd.array();
    QCOMPARE(qja.size(), sizes);

    for (int i = 0; i < qja.size(); i ++) {
      auto qjv = qja[i];
      QCOMPARE(qjv.type(), QJsonValue::Object);
      auto qjo = qjv.toObject();

      auto description = qjo.value("description");
      QCOMPARE(description.type(), QJsonValue::String);

      auto description_s = description.toString();
      QCOMPARE(description_s, "description #" + QString::number(i));

      auto fingerprint = qjo.value("fingerprint");
      QCOMPARE(fingerprint.type(), QJsonValue::String);

      auto fingerprint_s = fingerprint.toString();
      QCOMPARE(fingerprint_s, "fingerprint #" + QString::number(i));

      auto location = qjo.value("location");
      QCOMPARE(location.type(), QJsonValue::Object);
      auto location_o = location.toObject();

      auto location_path = location_o.value("path");
      QCOMPARE(location_path.type(), QJsonValue::String);

      auto location_path_o = location_path.toString();
      QCOMPARE(location_path_o, "location_path #" + QString::number(i));

      auto location_line = location_o.value("lines");
      QCOMPARE(location_line.type(), QJsonValue::Object);
      auto location_line_o = location_line.toObject();

      auto location_lval = location_line_o.value("begin");
      QCOMPARE(location_lval.type(), QJsonValue::Double);

      auto location_lval_i = location_lval.toInt();
      QCOMPARE(location_lval_i, i);
    }
  }

  void publish_reports_data() {
    QTest::addColumn<QStringList>("sections");
    QTest::addColumn<QStringList>("commands");

    QTest::addColumn<QString    >("stage"   );
    QTest::addColumn<int        >("sizes"   );

    QStringList stages({
      "during",
    });

    for (QString& stage : stages) {
      QTest::addRow("single single %s", qPrintable(stage))
          << QStringList({ "Mera Luna" })
          << QStringList({ "echo 1" })
          << stage
          << 1;
    }

    for (QString& stage : stages) {
      QTest::addRow("single double %s", qPrintable(stage))
          << QStringList({ "Mera Luna" })
          << QStringList({ "echo 2" })
          << stage
          << 2;
    }

    for (QString& stage : stages) {
      QTest::addRow("double single %s", qPrintable(stage))
          << QStringList({ "Mera Luna", "Star Dust" })
          << QStringList({ "echo 1" })
          << stage
          << 2;
    }

    for (QString& stage : stages) {
      QTest::addRow("double double %s", qPrintable(stage))
          << QStringList({ "Mera Luna", "Star Dust" })
          << QStringList({ "echo 2" })
          << stage
          << 4;
    }
  }

  void publish_depends() {
    QFETCH(QStringList, sections);
    QFETCH(QStringList, commands);

    QFETCH(QString    , stage   );
    QFETCH(int        , sizes   );

    QByteArray data = publish(depends_path_, sections, commands, stage);

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isObject());

    auto qjo = qjd.object();

    auto qjv_defi = qjo.value("dependency_files");
    QCOMPARE(qjv_defi.type(), QJsonValue::Array);
    auto qja_defi = qjv_defi.toArray();

    auto qjv_depe = qja_defi.first();
    QCOMPARE(qjv_depe.type(), QJsonValue::Object);
    auto qjo_depe = qjv_depe.toObject();

         qjv_depe = qjo_depe.value("dependencies");
    QCOMPARE(qjv_depe.type(), QJsonValue::Array);
    auto qja_depe = qjv_depe.toArray();

    QCOMPARE(qja_depe.size(), sizes);

    for (int i = 0; i < qja_depe.size(); i ++) {
      auto qjv_depe = qja_depe[i];
      QCOMPARE(qjv_depe.type(), QJsonValue::Object);
      auto qjo_depe = qjv_depe.toObject();

      auto qjv_pack = qjo_depe.value("package");
      QCOMPARE(qjv_pack.type(), QJsonValue::Object);
      auto qjo_pack = qjv_pack.toObject();

      auto qjv_name = qjo_pack.value("name");
      QCOMPARE(qjv_name.type(), QJsonValue::String);

      auto qjs_name = qjv_name.toString();
      QCOMPARE(qjs_name, "package #" + QString::number(i));

      auto qjv_vers = qjo_depe.value("version");
      QCOMPARE(qjv_vers.type(), QJsonValue::String);

      auto qjs_vers = qjv_vers.toString();
      QCOMPARE(qjs_vers, "version #" + QString::number(i));
    }
  }

  void publish_depends_data() {
    publish_reports_data();
  }

 private:
  QByteArray publish_once(const QString& path) {
    QByteArray qbytearray;

    [&] {
      configure([&] (QSettings& qsettings) {
        qsettings.setValue("reports_test_publish_analyzer", "Mera Luna");

        {
          qsettings.beginWriteArray("during");
          LVD_FINALLY { qsettings.endArray(); };

          qsettings.setArrayIndex(0);
          qsettings.setValue("during", "echo -1");

          qsettings.setArrayIndex(1);
          qsettings.setValue("during", "echo -1");

          qsettings.setArrayIndex(2);
          qsettings.setValue("during", "echo -1");
        }

        qsettings.endGroup();

        qsettings.setValue("source", "/");
        qsettings.setValue("binary", "/");

        qsettings.beginGroup("dummy");
      });

      Reports reports(config_);

      QSignalSpy spy(&reports, &Reports::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        reports.execute();
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);

      QFile qfile(path);
      qfile.open(QFile::ReadOnly);

      QVERIFY(qfile.isOpen());

      qbytearray = qfile.readAll();
    }();

    return qbytearray;
  }

 private slots:
  void publish_reports_once() {
    QByteArray data = publish_once(reports_path_);

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isArray());

    auto qja = qjd.array();
    QCOMPARE(qja.size(), 1);
  }

  void publish_depends_once() {
    QByteArray data = publish_once(depends_path_);

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isObject());

    auto qjo = qjd.object();

    auto qjv_defi = qjo.value("dependency_files");
    QCOMPARE(qjv_defi.type(), QJsonValue::Array);
    auto qja_defi = qjv_defi.toArray();

    auto qjv_depe = qja_defi.first();
    QCOMPARE(qjv_depe.type(), QJsonValue::Object);
    auto qjo_depe = qjv_depe.toObject();

         qjv_depe = qjo_depe.value("dependencies");
    QCOMPARE(qjv_depe.type(), QJsonValue::Array);
    auto qja_depe = qjv_depe.toArray();

    QCOMPARE(qja_depe.size(), 1);
  }

  void publish_reports_null() {
    configure([&] (QSettings& qsettings) {
      qsettings.setValue("reports_test_message_analyzer", "Mera Luna");

      {
        qsettings.beginWriteArray("during");
        LVD_FINALLY { qsettings.endArray(); };

        qsettings.setArrayIndex(0);
        qsettings.setValue("during", "echo Mera Luna");
      }
    });

    ReportsTestMessageAnalyzer::reports << Report(
      "Description", "Fingerprint", "LocationPath", "LocationLine"
    );

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QFile qfile(reports_path_);
    qfile.open(QFile::ReadOnly);

    QVERIFY(qfile.isOpen());

    QByteArray data = qfile.readAll();

    auto qjd = QJsonDocument::fromJson(data);
    QVERIFY (qjd.isArray());

    auto qja = qjd.array();
    QCOMPARE(qja.size(), 0);
  }

  // ----------

  void timeout() {
    QFETCH(QString, timeout);
    QFETCH(bool   , section);

    { ReportsTestExecuteAnalyzer::clear_lines();

      QSettings qsettings(config_, QSettings::IniFormat);
      qsettings.setFallbacksEnabled(false);

      if (!section) {
        qsettings.setValue(timeout, "1");
      }

      qsettings.beginGroup("Mera Luna");
      LVD_FINALLY { qsettings.endGroup(); };

      qsettings.setValue("before", "sleep 42");
      qsettings.setValue("during", "sleep 42");
      qsettings.setValue("beyond", "sleep 42");

      if ( section) {
        qsettings.setValue(timeout, "1");
      }
    }

    Reports reports(config_);

    QSignalSpy spy(&reports, &Reports::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      reports.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void timeout_data() {
    QTest::addColumn<QString>("timeout");
    QTest::addColumn<bool   >("section");

    QTest::addRow("hard_timeout global") << "hard_timeout" << false;
    QTest::addRow("hard_timeout custom") << "hard_timeout" << true;

    QTest::addRow("soft_timeout global") << "soft_timeout" << false;
    QTest::addRow("soft_timeout custom") << "soft_timeout" << true;
  }

  // ----------

 private:
  void configure(const QStringList&              sections,
                 std::function<void(QSettings&)> configure_cb) {
    QSettings qsettings(config_, QSettings::IniFormat);
    qsettings.setFallbacksEnabled(false);

    qsettings.setValue("report", reports_path_);

    qsettings.setValue("depends_path", depends_path_);

    for (const QString& section : sections) {
      qsettings.beginGroup(section);
      LVD_FINALLY { qsettings.endGroup(); };

      configure_cb(qsettings);
    }
  }

  void configure(std::function<void(QSettings&)> configure_cb) {
    configure({ "Mera Luna" }, configure_cb);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };

    ReportsTestExecuteAnalyzer::clear_lines();

    ReportsTestMessageAnalyzer::clear_reports();
    ReportsTestMessageAnalyzer::clear_depends();

    ReportsTestPublishAnalyzer::clear_sizes();
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }

 private:
  const QString config_ = "li.void.reports.conf";

  const QString reports_path_ = "reports.json";
  const QString depends_path_ = "depends.json";
};

LVD_TEST_MAIN(ReportsTest)
#include "reports_test.moc"  // IWYU pragma: keep
