/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "reports_test_analyzer.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

// ----------

ReportsTestExecuteAnalyzer::ReportsTestExecuteAnalyzer(QObject* parent)
    : Analyzer(parent) {}

ReportsTestExecuteAnalyzer::~ReportsTestExecuteAnalyzer() = default;

QStringList ReportsTestExecuteAnalyzer::lines;

// ----------

ReportsTestMessageAnalyzer::ReportsTestMessageAnalyzer(QObject* parent)
    : Analyzer(parent) {}

ReportsTestMessageAnalyzer::~ReportsTestMessageAnalyzer() = default;

Report::s ReportsTestMessageAnalyzer::reports;
Depend::s ReportsTestMessageAnalyzer::depends;

// ----------

ReportsTestPublishAnalyzer::ReportsTestPublishAnalyzer(QObject* parent)
    : Analyzer(parent) {}

ReportsTestPublishAnalyzer::~ReportsTestPublishAnalyzer() = default;

int         ReportsTestPublishAnalyzer::sizes = 0;
