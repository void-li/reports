/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

// ----------

int test(int i) {
  if (i == 0) {
    int j = 1 / i;
    return j;
  }

  return i;
}

int main(int argc, char* argv[]) {
  int* a = nullptr;
  int  b = *a;

  return test(0);
}
