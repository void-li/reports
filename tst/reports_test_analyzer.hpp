/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdlib>

#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QString>
#include <QStringList>
#include <QVector>

#include "analyzer.hpp"
#include "depend.hpp"
#include "report.hpp"
#include "vulnerability.hpp"

using namespace lvd;
using namespace lvd::reports;

// ----------

class ReportsTestExecuteAnalyzer : public Analyzer {
  Q_OBJECT

 public:
  ReportsTestExecuteAnalyzer        (QObject* parent = nullptr);
  ~ReportsTestExecuteAnalyzer() override;

  static
  ReportsTestExecuteAnalyzer* create(QObject* parent = nullptr) {
    return new ReportsTestExecuteAnalyzer(parent);
  }

 private:
  void analyze_impl(const QString& line) override {
    lines.append(line.trimmed());
  }

 public:
  static QStringList lines;
  static void clear_lines() {
    lines.clear();
  }
};

DECLARE_ANALYZER(ReportsTestExecuteAnalyzer, reports_test_execute_analyzer);

// ----------

class ReportsTestMessageAnalyzer : public Analyzer {
  Q_OBJECT

 public:
  ReportsTestMessageAnalyzer        (QObject* parent = nullptr);
  ~ReportsTestMessageAnalyzer() override;

  static
  ReportsTestMessageAnalyzer* create(QObject* parent = nullptr) {
    return new ReportsTestMessageAnalyzer(parent);
  }

 private:
  void analyze_impl(const QString& line) override {
    Q_UNUSED(line)

    for (const Report& report : qAsConst(reports)) {
      emit this->report(report);
    }

    for (const Depend& depend : qAsConst(depends)) {
      emit this->depend(depend);
    }
  }

 public:
  static Report::s reports;
  static void clear_reports() {
    reports.clear();
  }

  static Depend::s depends;
  static void clear_depends() {
    depends.clear();
  }
};

DECLARE_ANALYZER(ReportsTestMessageAnalyzer, reports_test_message_analyzer);

// ----------

class ReportsTestPublishAnalyzer : public Analyzer {
  Q_OBJECT

 public:
  ReportsTestPublishAnalyzer        (QObject* parent = nullptr);
  ~ReportsTestPublishAnalyzer() override;

  static
  ReportsTestPublishAnalyzer* create(QObject* parent = nullptr) {
    return new ReportsTestPublishAnalyzer(parent);
  }

 private:
  void analyze_impl(const QString& line) override {
    bool ok;

    int lint = line.toInt(&ok);

    if (!ok) {
      LVD_THROW_RUNTIME("invalid testdata: " + line);
    }

    for (int i = 0; i < std::abs(lint); i ++) {
      QString j;

      if (lint > 0) {
        j = QString::number(i + sizes);
      } else {
        j = QString::number(i        );
      }

      Report message(
        "description #"    + j,
        "fingerprint #"    + j,
        "/location_path #" + j,
                             j
      );

      emit this->report(message);

      Depend depend("package #" + j,
                    "version #" + j,
                    {});

      emit this->depend(depend);
    }

    sizes += lint;
  }

 public:
  static int         sizes;
  static void clear_sizes() {
    sizes = 0;
  }
};

DECLARE_ANALYZER(ReportsTestPublishAnalyzer, reports_test_publish_analyzer);
