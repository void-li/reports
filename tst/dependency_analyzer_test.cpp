/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QDir>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QString>
#include <QStringList>

#include "lvd/metatype.hpp"

#include "dependency_analyzer.hpp"
#include "test_config.hpp"
#include "vulnerability.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::reports;

// ----------

namespace {
// clazy:excludeall=non-pod-global-static

const QString gmake_mera =
"Scanning dependencies of target mera\n"
"[ 12%] Building CXX object CMakeFiles/mera.dir/mera.cpp.o\n"
"[ 25%] Linking CXX executable mera\n"
"[ 25%] Built target mera\n"
;

const QString gmake_luna =
"Scanning dependencies of target luna\n"
"[ 62%] Building CXX object CMakeFiles/luna.dir/luna.cpp.o\n"
"[ 75%] Linking CXX shared library libluna.so\n"
"[ 75%] Built target luna\n"
;

const QString gmake_moon =
"Scanning dependencies of target moon\n"
"[ 37%] Building CXX object CMakeFiles/moon.dir/moon.cpp.o\n"
"[ 50%] Linking CXX static library libmoon.a\n"
"[ 50%] Built target moon\n"
;

const QString gmake_star =
"Scanning dependencies of target star\n"
"[ 87%] Building CXX object star/CMakeFiles/star.dir/star.cpp.o\n"
"[100%] Linking CXX shared library libstar.so\n"
"[100%] Built target star\n"
;

const QString ninja_mera =
"[1/8] Building CXX object CMakeFiles/mera.dir/mera.cpp.o\n"
"[2/8] Linking CXX executable mera\n"
;

const QString ninja_luna =
"[3/8] Building CXX object CMakeFiles/luna.dir/luna.cpp.o\n"
"[4/8] Linking CXX shared library libluna.so\n"
;

const QString ninja_moon =
"[5/8] Building CXX object CMakeFiles/moon.dir/moon.cpp.o\n"
"[6/8] Linking CXX static library libmoon.a\n"
;

const QString ninja_star =
"[7/8] Building CXX object star/CMakeFiles/star.dir/star.cpp.o\n"
"[8/8] Linking CXX shared library star/libstar.so\n"
;

}  // namespace

// ----------

class DependencyAnalyzerTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    DependencyAnalyzer        ();
    DependencyAnalyzer::create()->deleteLater();
  }

  // ----------

  void gmake() {
    QFETCH(QString, output);
    QFETCH(bool   , result);

    DependencyAnalyzer dependency_analyzer;

    QSignalSpy spy(&dependency_analyzer, &DependencyAnalyzer::depend);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = output.split("\n");

      for (const QString& line : lines) {
        dependency_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    if (result) {
      QVERIFY (spy.wait(256));
      QVERIFY (spy.size()> 0);
    } else {
      QVERIFY(!spy.wait(256));
      QCOMPARE(spy.size(), 0);
    }
  }

  void gmake_data() {
    QTest::addColumn<QString>("output");
    QTest::addColumn<bool   >("result");

    QTest::addRow("") << gmake_mera << true;
    QTest::addRow("") << gmake_luna << true;
    QTest::addRow("") << gmake_moon << false;
    QTest::addRow("") << gmake_star << true;
  }

  // ----------

  void ninja() {
    QFETCH(QString, output);
    QFETCH(bool   , result);

    DependencyAnalyzer dependency_analyzer;

    QSignalSpy spy(&dependency_analyzer, &DependencyAnalyzer::depend);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = output.split("\n");

      for (const QString& line : lines) {
        dependency_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    if (result) {
      QVERIFY (spy.wait(256));
      QVERIFY (spy.size()> 0);
    } else {
      QVERIFY(!spy.wait(256));
      QCOMPARE(spy.size(), 0);
    }
  }

  void ninja_data() {
    QTest::addColumn<QString>("output");
    QTest::addColumn<bool   >("result");

    QTest::addRow("executable") << ninja_mera << true;
    QTest::addRow("shared lib") << ninja_luna << true;
    QTest::addRow("static lib") << ninja_moon << false;
    QTest::addRow("nested dir") << ninja_star << true;
  }

  // ----------

 private slots:
  void init() {
    Test::init();

    QString data_path = Current_Binary_Dir() + "/dependency_analyzer_test_data";
    QVERIFY(QDir::setCurrent(data_path));
  }

  void initTestCase() {
    Test::initTestCase();

    Metatype::metatype();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
    Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(DependencyAnalyzerTest)
#include "dependency_analyzer_test.moc"  // IWYU pragma: keep
