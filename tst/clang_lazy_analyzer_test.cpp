/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QDir>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QTemporaryDir>
#include <QVariant>

#include "lvd/metatype.hpp"

#include "clang_lazy_analyzer.hpp"
#include "report.hpp"
#include "test_config.hpp"
#include "vulnerability.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::reports;

// ----------

namespace {
// clazy:excludeall=non-pod-global-static

const QString singlex_data =
"/clang_lazy_analyzer_test/main.cpp:18:52: warning: captured local variable by reference might go out of scope before lambda is called [-Wclazy-lambda-in-connect]\n"
"  QObject::connect(nullptr, &QObject::destroyed, [&a] { });\n"
"                                                   ^\n"
;

const QString singlex_data_mesg0 = "captured local variable by reference might go out of scope before lambda is called [-Wclazy-lambda-in-connect]";
const QString singlex_data_path0 = "/clang_lazy_analyzer_test/main.cpp";
const QString singlex_data_line0 = "18";

const QString several_data =
"/clang_lazy_analyzer_test/main.cpp:18:52: warning: captured local variable by reference might go out of scope before lambda is called [-Wclazy-lambda-in-connect]\n"
"  QObject::connect(nullptr, &QObject::destroyed, [&a] { });\n"
"                                                   ^\n"
"/clang_lazy_analyzer_test/main.cpp:21:10: warning: Use midRef() instead [-Wclazy-qstring-ref]\n"
"  b.mid(1).toInt();\n"
"         ^\n"
"/clang_lazy_analyzer_test/main.cpp:23:43: warning: Use multi-arg instead [-Wclazy-qstring-arg]\n"
"  const QString c = QString(\" %2\").arg(b).arg(b);\n"
"                                          ^\n"
;

const QString several_data_mesg0 = "captured local variable by reference might go out of scope before lambda is called [-Wclazy-lambda-in-connect]";
const QString several_data_path0 = "/clang_lazy_analyzer_test/main.cpp";
const QString several_data_line0 = "18";

const QString several_data_mesg1 = "Use midRef() instead [-Wclazy-qstring-ref]";
const QString several_data_path1 = "/clang_lazy_analyzer_test/main.cpp";
const QString several_data_line1 = "21";

const QString several_data_mesg2 = "Use multi-arg instead [-Wclazy-qstring-arg]";
const QString several_data_path2 = "/clang_lazy_analyzer_test/main.cpp";
const QString several_data_line2 = "23";

const QString complex_data =
"[ 50%] Building CXX object CMakeFiles/main.dir/main.cpp.o\n"
"/clang_lazy_analyzer_test/main.cpp:19:52: warning: captured local variable by reference might go out of scope before lambda is called [-Wclazy-lambda-in-connect]\n"
"  QObject::connect(nullptr, &QObject::destroyed, [&a] { });\n"
"                                                   ^\n"
"/clang_lazy_analyzer_test/main.cpp:22:10: warning: Use midRef() instead [-Wclazy-qstring-ref]\n"
"  b.mid(1).toInt();\n"
"         ^\n"
"/clang_lazy_analyzer_test/main.cpp:24:43: warning: Use multi-arg instead [-Wclazy-qstring-arg]\n"
"  const QString c = QString(\"%1 %2\").arg(b).arg(b);\n"
"                                          ^\n"
"3 warnings generated.\n"
"[100%] Linking CXX executable main\n"
"[100%] Built target main\n"
;

const QString complex_data_mesg0 = "captured local variable by reference might go out of scope before lambda is called [-Wclazy-lambda-in-connect]";
const QString complex_data_path0 = "/clang_lazy_analyzer_test/main.cpp";
const QString complex_data_line0 = "19";

const QString complex_data_mesg1 = "Use midRef() instead [-Wclazy-qstring-ref]";
const QString complex_data_path1 = "/clang_lazy_analyzer_test/main.cpp";
const QString complex_data_line1 = "22";

const QString complex_data_mesg2 = "Use multi-arg instead [-Wclazy-qstring-arg]";
const QString complex_data_path2 = "/clang_lazy_analyzer_test/main.cpp";
const QString complex_data_line2 = "24";

}  // namespace

// ----------

class ClangLazyAnalyzerTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ClangLazyAnalyzer        ();
    ClangLazyAnalyzer::create()->deleteLater();
  }

  // ----------

  void singlex() {
    ClangLazyAnalyzer clang_lazy_analyzer;

    QSignalSpy spy(&clang_lazy_analyzer, &ClangLazyAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = singlex_data.split("\n");
      for (const QString& line : lines) {
        clang_lazy_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].value<Report>().description()  , singlex_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), singlex_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), singlex_data_line0);
  }

  void several() {
    ClangLazyAnalyzer clang_lazy_analyzer;

    QSignalSpy spy(&clang_lazy_analyzer, &ClangLazyAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = several_data.split("\n");
      for (const QString& line : lines) {
        clang_lazy_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 3);

    QCOMPARE(spy[0][0].value<Report>().description()  , several_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), several_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), several_data_line0);

    QCOMPARE(spy[1][0].value<Report>().description()  , several_data_mesg1);
    QCOMPARE(spy[1][0].value<Report>().location_path(), several_data_path1);
    QCOMPARE(spy[1][0].value<Report>().location_line(), several_data_line1);

    QCOMPARE(spy[2][0].value<Report>().description()  , several_data_mesg2);
    QCOMPARE(spy[2][0].value<Report>().location_path(), several_data_path2);
    QCOMPARE(spy[2][0].value<Report>().location_line(), several_data_line2);
  }

  void complex() {
    ClangLazyAnalyzer clang_lazy_analyzer;

    QSignalSpy spy(&clang_lazy_analyzer, &ClangLazyAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = complex_data.split("\n");
      for (const QString& line : lines) {
        clang_lazy_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 3);

    QCOMPARE(spy[0][0].value<Report>().description()  , complex_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), complex_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), complex_data_line0);

    QCOMPARE(spy[1][0].value<Report>().description()  , complex_data_mesg1);
    QCOMPARE(spy[1][0].value<Report>().location_path(), complex_data_path1);
    QCOMPARE(spy[1][0].value<Report>().location_line(), complex_data_line1);

    QCOMPARE(spy[2][0].value<Report>().description()  , complex_data_mesg2);
    QCOMPARE(spy[2][0].value<Report>().location_path(), complex_data_path2);
    QCOMPARE(spy[2][0].value<Report>().location_line(), complex_data_line2);
  }

  // ----------

  void calling() {
    QTemporaryDir qtemporarydir;
    QVERIFY(qtemporarydir.isValid());

    QString current_path = QDir::currentPath();
    QDir::setCurrent(qtemporarydir.path());

    LVD_FINALLY {
      QDir::setCurrent(current_path);
    };

    int ret;

    ret = execute("cmake", "-GNinja", "-DCMAKE_CXX_COMPILER=clazy", Current_Source_Dir() + "/clang_lazy_analyzer_test");
    QCOMPARE(ret, 0);

    ret = execute("ninja");
    QCOMPARE(ret, 0);

    execute_stdout_.replace(Current_Source_Dir().toLocal8Bit(), "");
    execute_stderr_.replace(Current_Source_Dir().toLocal8Bit(), "");

    ClangLazyAnalyzer clang_lazy_analyzer;

    QSignalSpy spy(&clang_lazy_analyzer, &ClangLazyAnalyzer::report);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      const QStringList lines = QString::fromLocal8Bit(execute_stdout_).split('\n');
      for (const QString& line : lines) {
        clang_lazy_analyzer.analyze(line);
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 3);

    QCOMPARE(spy[0][0].value<Report>().description()  , complex_data_mesg0);
    QCOMPARE(spy[0][0].value<Report>().location_path(), complex_data_path0);
    QCOMPARE(spy[0][0].value<Report>().location_line(), complex_data_line0);

    QCOMPARE(spy[1][0].value<Report>().description()  , complex_data_mesg1);
    QCOMPARE(spy[1][0].value<Report>().location_path(), complex_data_path1);
    QCOMPARE(spy[1][0].value<Report>().location_line(), complex_data_line1);

    QCOMPARE(spy[2][0].value<Report>().description()  , complex_data_mesg2);
    QCOMPARE(spy[2][0].value<Report>().location_path(), complex_data_path2);
    QCOMPARE(spy[2][0].value<Report>().location_line(), complex_data_line2);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();

    Metatype::metatype();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ClangLazyAnalyzerTest)
#include "clang_lazy_analyzer_test.moc"  // IWYU pragma: keep
